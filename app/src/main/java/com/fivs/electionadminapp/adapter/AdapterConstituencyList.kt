package com.fivs.electionadminapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fivs.electionadminapp.R
import com.fivs.electionadminapp.callback.ConstituencyClickCallback
import com.fivs.electionadminapp.data.model.ConstituencyModel
import kotlinx.android.synthetic.main.item_constituency_list.view.*


class AdapterConstituencyList(
    var context: Context,
    private var callback: ConstituencyClickCallback,
    var mList: ArrayList<ConstituencyModel>
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<AdapterConstituencyList.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(context, callback, mList[position])
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_constituency_list, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {


        fun onBind(
            context: Context,
            callback: ConstituencyClickCallback,
            data: ConstituencyModel
        ) {


            var requestOptions = RequestOptions()
            requestOptions =
                requestOptions.circleCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)
            Glide.with(context)
                .load(data.image)
                .apply(requestOptions)
                .into(itemView.ivImage)

            itemView.tvName.text = data.name
            itemView.tvStateName.text = data.state
            itemView.swBlocked.isChecked = data.isInactive == "1"
            itemView.swBlocked.setOnTouchListener(OnTouchListener { v, event -> event.actionMasked == MotionEvent.ACTION_MOVE })
            itemView.root.setOnClickListener {

                callback.onItemClick(position = adapterPosition)

            }
            itemView.ivImage.setOnClickListener {

                callback.onImageClick(position = adapterPosition)

            }

        }
    }

}