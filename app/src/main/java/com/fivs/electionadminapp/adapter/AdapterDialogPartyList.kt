package com.fivs.electionadminapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fivs.electionadminapp.R
import com.fivs.electionadminapp.callback.ConstituencyClickCallback
import com.fivs.electionadminapp.callback.PartyClickCallback
import com.fivs.electionadminapp.callback.StateClickCallback
import com.fivs.electionadminapp.data.model.ConstituencyModel
import com.fivs.electionadminapp.data.model.PartyModel
import com.fivs.electionadminapp.data.model.StateModel
import kotlinx.android.synthetic.main.item_dialog_state_list.view.*

class AdapterDialogPartyList(
    var context: Context,
    private var callback: PartyClickCallback,
    var mList: ArrayList<PartyModel>
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<AdapterDialogPartyList.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(context, callback, mList[position])
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_dialog_state_list, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {


        fun onBind(
            context: Context,
            callback: PartyClickCallback,
            data: PartyModel
        ) {



            itemView.tvName.text = data.name
            itemView.root.setOnClickListener {

                callback.onItemClick(position = adapterPosition)

            }


        }
    }

}