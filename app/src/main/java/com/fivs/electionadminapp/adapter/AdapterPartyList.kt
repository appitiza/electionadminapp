package com.fivs.electionadminapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fivs.electionadminapp.R
import com.fivs.electionadminapp.callback.PartyClickCallback
import com.fivs.electionadminapp.data.model.PartyModel
import kotlinx.android.synthetic.main.item_party_list.view.*

class AdapterPartyList(
    var context: Context,
    private var callback: PartyClickCallback,
    var mList: ArrayList<PartyModel>
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<AdapterPartyList.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(context, callback, mList[position])
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_party_list, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {


        fun onBind(
            context: Context,
            callback: PartyClickCallback,
            data: PartyModel
        ) {


            var requestOptions = RequestOptions()
            requestOptions =
                requestOptions.circleCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)
            Glide.with(context)
                .load(data.image)
                .apply(requestOptions)
                .into(itemView.ivImage)

            itemView.tvName.text = data.name
            itemView.tvPartyStateName.text = data.state
            if (data.isRegional == "1") {
                itemView.tvRegional.visibility = View.VISIBLE
            } else {
                itemView.tvRegional.visibility = View.GONE
            }
            itemView.root.setOnClickListener {

                callback.onItemClick(position = adapterPosition)

            }
            itemView.ivImage.setOnClickListener {

                callback.onImageClick(position = adapterPosition)

            }

        }
    }

}