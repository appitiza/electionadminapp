package com.fivs.electionadminapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fivs.electionadminapp.R
import com.fivs.electionadminapp.callback.StaffClickCallback
import com.fivs.electionadminapp.data.model.StaffModel
import kotlinx.android.synthetic.main.item_staff_list.view.*

class AdapterStaffList(
    var context: Context,
    private var callback: StaffClickCallback,
    var mList: ArrayList<StaffModel>
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<AdapterStaffList.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(context, callback, mList[position])
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_staff_list, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {


        fun onBind(
            context: Context,
            callback: StaffClickCallback,
            data: StaffModel
        ) {

            val displayName = StringBuilder()
            displayName.append(data.name)
            displayName.append("[")
            displayName.append(data.id)
            displayName.append("]")
            itemView.tvName.text = displayName.toString()
            itemView.tvConstituency.text = data.constituencyname
            itemView.tvState.text = data.statename
            itemView.root.setOnClickListener {

                callback.onItemClick(position = adapterPosition)

            }

        }
    }

}