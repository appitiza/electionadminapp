package com.fivs.electionadminapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fivs.electionadminapp.R
import com.fivs.electionadminapp.callback.StateClickCallback
import com.fivs.electionadminapp.data.model.StateModel
import kotlinx.android.synthetic.main.item_state_list.view.*

class AdapterStateList(
    var context: Context,
    private var callback: StateClickCallback,
    var mList: ArrayList<StateModel>
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<AdapterStateList.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(context, callback, mList[position])
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_state_list, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {


        fun onBind(
            context: Context,
            callback: StateClickCallback,
            data: StateModel
        ) {


            var requestOptions = RequestOptions()
            requestOptions =
                requestOptions.circleCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)
            Glide.with(context)
                .load(data.image)
                .apply(requestOptions)
                .into(itemView.ivImage)

            itemView.tvName.text = data.name
            itemView.root.setOnClickListener {

                callback.onItemClick(position = adapterPosition)

            }
            itemView.ivImage.setOnClickListener {

                callback.onImageClick(position = adapterPosition)

            }

        }
    }

}