package com.fivs.electionadminapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fivs.electionadminapp.R
import com.fivs.electionadminapp.callback.VoterClickCallback
import com.fivs.electionadminapp.data.model.VoterModel
import kotlinx.android.synthetic.main.item_voter_list.view.*

class AdapterVoterList(
    var context: Context,
    private var callback: VoterClickCallback,
    var mList: ArrayList<VoterModel>
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<AdapterVoterList.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(context, callback, mList[position])
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_voter_list, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {


        fun onBind(
            context: Context,
            callback: VoterClickCallback,
            data: VoterModel
        ) {

            itemView.tvAdhaar.text = data.id
            itemView.tvName.text = data.name
            itemView.root.setOnClickListener {

                callback.onItemClick(position = adapterPosition)

            }

        }
    }

}