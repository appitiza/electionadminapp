package com.fivs.electionadminapp.callback

interface CandidateClickCallback {
    fun onItemClick(position: Int)
    fun onImageClick(position: Int)
    fun onItemRemoveClick(position: Int)
}