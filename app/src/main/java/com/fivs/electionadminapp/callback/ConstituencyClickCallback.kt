package com.fivs.electionadminapp.callback

interface ConstituencyClickCallback {
    fun onItemClick(position: Int)
    fun onImageClick(position: Int)
    fun onItemRemoveClick(position: Int)
}