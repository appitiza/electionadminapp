package com.fivs.electionadminapp.callback

interface PartyClickCallback {
    fun onItemClick(position: Int)
    fun onImageClick(position: Int)
    fun onItemRemoveClick(position: Int)
}