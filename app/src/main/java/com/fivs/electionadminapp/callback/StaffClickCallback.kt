package com.fivs.electionadminapp.callback

interface StaffClickCallback {
    fun onItemClick(position: Int)
    fun onImageClick(position: Int)
    fun onItemRemoveClick(position: Int)
}