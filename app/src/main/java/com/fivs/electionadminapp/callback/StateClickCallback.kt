package com.fivs.electionadminapp.callback

interface StateClickCallback {
    fun onItemClick(position: Int)
    fun onImageClick(position: Int)
    fun onItemRemoveClick(position: Int)
}