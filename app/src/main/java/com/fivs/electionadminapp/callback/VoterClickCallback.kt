package com.fivs.electionadminapp.callback

interface VoterClickCallback {
    fun onItemClick(position: Int)
    fun onImageClick(position: Int)
    fun onItemRemoveClick(position: Int)
}