package com.fivs.electionadminapp.data.model

import java.io.Serializable

open class ResultPartyModel:Serializable {
    var id : String = ""
    var cast_vote : Int = 0
}