package com.fivs.electionadminapp.data.model

import java.io.Serializable

open class ResultStateModel:Serializable {
    var id : String = ""
    var cast_vote : Int = 0
    var voter_count : Int = 0
    var won_party : String = ""
    var won_party_id : String = ""
    var won_lead : Int = 0

}