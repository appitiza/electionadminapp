package com.fivs.electionadminapp.data.model

import java.io.Serializable

open class StaffModel:Serializable {
    var id : String = ""
    var fingerimage : String = ""
    var name : String = ""
    var mobile : String = ""
    var stateid : String = ""
    var statename : String = ""
    var constituencyid : String = ""
    var constituencyname : String = ""
    var enrolledTemplete : ByteArray = byteArrayOf()
}