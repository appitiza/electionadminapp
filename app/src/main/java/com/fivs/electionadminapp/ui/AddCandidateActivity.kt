package com.fivs.electionadminapp.ui

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fivs.electionadminapp.R
import com.fivs.electionadminapp.adapter.AdapterDialogConstituencyList
import com.fivs.electionadminapp.adapter.AdapterDialogPartyList
import com.fivs.electionadminapp.adapter.AdapterDialogStateList
import com.fivs.electionadminapp.callback.ConstituencyClickCallback
import com.fivs.electionadminapp.callback.PartyClickCallback
import com.fivs.electionadminapp.callback.StateClickCallback
import com.fivs.electionadminapp.data.model.CandidateModel
import com.fivs.electionadminapp.data.model.ConstituencyModel
import com.fivs.electionadminapp.data.model.PartyModel
import com.fivs.electionadminapp.data.model.StateModel
import com.fivs.electionadminapp.util.Constants
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_add_candidate.*
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class AddCandidateActivity : BaseActivity() {

    private var PICK_IMAGE: Int = 0
    private var PICK_LOGO: Int = 1

    private var CAPTURE_IMAGE: Int = 2
    private var CAPTURE_LOGO: Int = 3

    private lateinit var photoURI: Uri
    private lateinit var photoFile: File
    private var imagePath = ""
    private val REQUEST_PERMISSION = 3

    private var mImageImage: Uri? = Uri.EMPTY
    private var mImageLogo: Uri? = Uri.EMPTY
    private lateinit var mImageFile: File
    private lateinit var mLogoFile: File

    private var mIsEdit = false
    private lateinit var mModel: CandidateModel
    private lateinit var mStorageRef: StorageReference
    private var loading = false
    private var mStateDataList: ArrayList<StateModel> = arrayListOf()
    private var mSelectedState: StateModel = StateModel()
    private var mConstituencyDataList: ArrayList<ConstituencyModel> = arrayListOf()
    private var mSelectedConstituency: ConstituencyModel = ConstituencyModel()
    private var mPartyDataList: ArrayList<PartyModel> = arrayListOf()
    private var mCandidateList: ArrayList<CandidateModel> = arrayListOf()
    private var mSelectedParty: PartyModel = PartyModel()
    private var mStateChanged = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_candidate)
        setScreenTitle(R.string.add_candidate)
        initializeFirebase()
        getIntentData()
        enableBack()
        checkAndRequestPermissions()
        setClick()
        if (isNetworkAvailable()) {
            getStates()
        } else {
            showMessage(this, getString(R.string.no_network))
        }
    }

    private fun initializeFirebase() {
        mStorageRef = FirebaseStorage.getInstance().reference
    }

    private fun getIntentData() {
        if (intent != null) {
            if (intent.extras != null && intent.extras!!.getSerializable("data") != null) {
                mModel = intent.extras!!.getSerializable("data") as CandidateModel
                mIsEdit = true
                var requestOptions = RequestOptions()
                requestOptions =
                    requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)

                Glide.with(this)
                    .load(mModel.image)
                    .apply(requestOptions)
                    .into(ivImage)


                Glide.with(this)
                    .load(mModel.logo)
                    .apply(requestOptions)
                    .into(ivLogo)

                if (mIsEdit) {
                    etName.setText(mModel.name)
                    btnAdd.text = getString(R.string.update)
                    if (mModel.party == "other") {
                        llLogo.visibility = View.VISIBLE
                    } else {
                        llLogo.visibility = View.GONE
                    }
                    setScreenTitle(R.string.edit_candidate)
                }
            }
        }
    }

    private fun setClick() {
        ivImage.setOnClickListener {
            pickImage(true)
        }
        ivLogo.setOnClickListener {
            pickImage(false)
        }
        btnAdd.setOnClickListener {
            if (isNetworkAvailable()) {
                if (mIsEdit) {

                    if (::mImageFile.isInitialized && ::mLogoFile.isInitialized) {
                        if (etName.text.toString().trim().isNotEmpty()
                            && mSelectedState.id.isNotEmpty()
                            && mSelectedConstituency.id.isNotEmpty()
                            && mSelectedParty.id.isNotEmpty()
                        ) {
                            uploadImage()
                        } else {
                            showMessage(this, getString(R.string.fill_all))
                        }
                    } else if (::mImageFile.isInitialized && !::mLogoFile.isInitialized) {
                        if (etName.text.toString().trim().isNotEmpty()
                            && mSelectedState.id.isNotEmpty()
                            && mSelectedConstituency.id.isNotEmpty()
                            && mSelectedParty.id.isNotEmpty()
                        ) {
                            uploadImage()
                        }

                    } else if (!::mImageFile.isInitialized && ::mLogoFile.isInitialized) {
                        if (etName.text.toString().trim().isNotEmpty()
                            && mSelectedState.id.isNotEmpty()
                            && mSelectedConstituency.id.isNotEmpty()
                            && mSelectedParty.id.isNotEmpty()
                        ) {
                            if (mSelectedParty.name == "other") {
                                uploadLogo(mModel.image)
                            } else {
                                updateData(mSelectedParty.image, mModel.image)
                            }

                        }

                    } else {
                        if (etName.text.toString().trim().isNotEmpty() && mSelectedState.id.isNotEmpty()) {
                            updateData(mSelectedParty.image, mModel.image)
                        } else {
                            showMessage(this, getString(R.string.fill_all))
                        }
                    }
                } else {
                    if (mCandidateList.isEmpty() || mCandidateList.filter { it.partyid == mSelectedParty.id }.isEmpty()) {
                        if (::mImageFile.isInitialized) {
                            if (etName.text.toString().trim().isNotEmpty()
                                && mSelectedState.id.isNotEmpty()
                                && mSelectedConstituency.id.isNotEmpty()
                                && mSelectedParty.id.isNotEmpty()
                            ) {
                                uploadImage()
                            } else {
                                showMessage(this, getString(R.string.fill_all))
                            }
                        } else {
                            showMessage(this, getString(R.string.image_missing))
                        }
                    } else {
                        showMessage(this, getString(R.string.fill_all))
                    }
                }
            } else {
                showMessage(this, getString(R.string.no_network))
            }


        }
        llState.setOnClickListener {
            if (mStateDataList.isNotEmpty())
                pickState()
        }
        llConstituency.setOnClickListener {
            if (mConstituencyDataList.isNotEmpty()) {
                pickConstituency()
            }
        }
        llParty.setOnClickListener {
            if (mPartyDataList.isNotEmpty()) {
                pickParty()
            }
        }
    }

    private fun uploadImage() {
        if (mImageFile.exists()) {
            showProgressDialog(getString(R.string.processing))
            loading = true
            val remoteImagepath = StringBuilder()
            remoteImagepath.append(FirebaseFirestore.getInstance().collection(Constants.COLLECTION_PARTY).document().id + "_" + etName.text.toString())
            remoteImagepath.append(".jpg")
            val currentRef =
                mStorageRef.child(Constants.COLLECTION_CANDIDATE).child(remoteImagepath.toString())


            val stream = FileInputStream(mImageFile)

            val uploadTask = currentRef.putStream(stream)




            uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        Toast.makeText(this, task.exception.toString(), Toast.LENGTH_LONG).show()
                    }
                }
                currentRef.downloadUrl
            }.addOnCompleteListener { task ->
                dismissProgressDialogPopup()
                loading = false
                if (task.isSuccessful) {
                    val downloadUri = task.result

                    if (mIsEdit) {
                        if (::mLogoFile.isInitialized) {
                            if (etName.text.toString().trim().isNotEmpty()
                                && mSelectedState.id.isNotEmpty()
                                && mSelectedConstituency.id.isNotEmpty()
                                && mSelectedParty.id.isNotEmpty()
                            ) {


                                if (mSelectedParty.name == "other") {
                                    uploadLogo(downloadUri.toString())
                                } else {
                                    updateData(mSelectedParty.image, downloadUri.toString())
                                }
                            } else {
                                showMessage(this, getString(R.string.fill_all))
                            }
                        } else {
                            if (etName.text.toString().trim().isNotEmpty()
                                && mSelectedState.id.isNotEmpty()
                                && mSelectedConstituency.id.isNotEmpty()
                                && mSelectedParty.id.isNotEmpty()
                            ) {
                                updateData(mSelectedParty.image, downloadUri.toString())
                            } else {
                                showMessage(this, getString(R.string.fill_all))
                            }
                        }
                    } else {

                        if (mSelectedParty.name == "other") {
                            uploadLogo(downloadUri.toString())
                        } else {
                            uploadData(mSelectedParty.image, downloadUri.toString())
                        }


                    }

                } else {
                    Toast.makeText(this, "Failed", Toast.LENGTH_LONG).show()
                }
            }
        } else {
            showMessage(this, getString(R.string.issue_inaccessing_file))
        }

    }

    private fun uploadLogo(imageUrl: String) {

        showProgressDialog(getString(R.string.processing))
        loading = true
        val remoteImagepath = StringBuilder()
        remoteImagepath.append(FirebaseFirestore.getInstance().collection(Constants.COLLECTION_PARTY).document().id + "_" + etName.text.toString().trim())
        remoteImagepath.append(".jpg")
        val currentRef =
            mStorageRef.child(Constants.LOGO).child(remoteImagepath.toString())


        val stream = FileInputStream(mLogoFile)

        val uploadTask = currentRef.putStream(stream)




        uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    Toast.makeText(this, task.exception.toString(), Toast.LENGTH_LONG).show()
                }
            }
            currentRef.downloadUrl
        }.addOnCompleteListener { task ->
            dismissProgressDialogPopup()
            loading = false
            if (task.isSuccessful) {
                val downloadUri = task.result

                if (mIsEdit) {
                    updateData(downloadUri.toString(), imageUrl)
                } else {
                    uploadData(downloadUri.toString(), imageUrl)
                }

            } else {
                Toast.makeText(this, "Failed", Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun updateData(logo: String, image: String) {
        showProgressDialog(getString(R.string.processing))
        loading = true
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val map = HashMap<String, Any>()
        map[Constants.CANDIDATE_NAME] = etName.text.toString().trim()
        map[Constants.CANDIDATE_IMAGE] = image
        map[Constants.LOGO] = logo
        map[Constants.STATE_ID] = mSelectedState.id
        map[Constants.CONSTITUENCY_ID] = mSelectedConstituency.id
        map[Constants.PARTY_ID] = mSelectedParty.id
        map[Constants.PARTY_NAME] = mSelectedParty.name
        map[Constants.DATE] = FieldValue.serverTimestamp()

        db!!.collection(Constants.COLLECTION_CANDIDATE)
            .document(mModel.id)
            .set(map)
            .addOnCompleteListener { reg_task ->
                dismissProgressDialogPopup()
                loading = false
                if (reg_task.isSuccessful) {
                    showDoneMessage()
                } else {
                    showMessage(this, getString(R.string.failed))
                }
            }
    }

    private fun uploadData(logo: String, image: String) {

        showProgressDialog(getString(R.string.processing))
        loading = true
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val map = HashMap<String, Any>()
        map[Constants.CANDIDATE_NAME] = etName.text.toString().trim()
        map[Constants.CANDIDATE_IMAGE] = image
        map[Constants.LOGO] = logo
        map[Constants.STATE_ID] = mSelectedState.id
        map[Constants.CONSTITUENCY_ID] = mSelectedConstituency.id
        map[Constants.PARTY_ID] = mSelectedParty.id
        map[Constants.PARTY_NAME] = mSelectedParty.name
        map[Constants.DATE] = FieldValue.serverTimestamp()

        db!!.collection(Constants.COLLECTION_CANDIDATE)
            .add(map)
            .addOnCompleteListener { reg_task ->
                dismissProgressDialogPopup()
                loading = false
                if (reg_task.isSuccessful) {
                    showDoneMessage()
                } else {
                    showMessage(this, getString(R.string.failed))
                }
            }
    }

    private fun getStates() {


        loading = true
        showProgressDialog(getString(R.string.processing))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_STATE)
        val query = ref
            .orderBy(Constants.DATE, Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                val mList: ArrayList<StateModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    Log.d(" data", document.id + " => " + document.data)
                    val mCategoryData =
                        StateModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.STATE_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.STATE_NAME)).toString()
                    mList.add(mCategoryData)
                    if (mIsEdit && !mStateChanged) {
                        if (mModel.stateid == mCategoryData.id) {
                            mSelectedState = mCategoryData
                        }
                    }

                }
                mStateDataList.addAll(mList)
                if (mStateDataList.isEmpty()) {
                    tvStateName.text = getString(R.string.no_state_available)
                    tvStateChange.visibility = View.GONE
                } else {
                    if (mSelectedState.id == "") {
                        mSelectedState = mStateDataList[0]

                    }
                    tvStateName.text = mSelectedState.name
                    getParty(mSelectedState.id)


                    tvStateChange.visibility = View.VISIBLE
                }

            }
            .addOnCompleteListener {
                loading = false
                dismissProgressDialogPopup()
            }

    }

    private fun getConstituency(stateID: String) {


        loading = true
        mConstituencyDataList.clear()
        showProgressDialog(getString(R.string.processing))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_CONSTITUENCY)
        val query = ref
            .whereEqualTo(Constants.STATE_ID, stateID)
            .orderBy(Constants.DATE, Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                val mList: ArrayList<ConstituencyModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    Log.d(" data", document.id + " => " + document.data)
                    val mCategoryData =
                        ConstituencyModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.CONSTITUENCY_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.CONSTITUENCY_NAME)).toString()
                    mCategoryData.stateid =
                        (document.data?.get(Constants.STATE_ID)).toString()
                    mCategoryData.state =
                        (document.data?.get(Constants.STATE_NAME)).toString()
                    mList.add(mCategoryData)

                    if (mIsEdit && !mStateChanged) {
                        if (mModel.constituencyid == mCategoryData.id) {
                            mSelectedConstituency = mCategoryData
                        }
                    }
                }
                mConstituencyDataList.addAll(mList)
                if (mConstituencyDataList.isEmpty()) {
                    tvConstituencyName.text = getString(R.string.no_constituency_available)
                    tvConstituencyChange.visibility = View.GONE
                } else {

                    if (mSelectedConstituency.id == "") {
                        mSelectedConstituency = mConstituencyDataList[0]

                    }
                    tvConstituencyName.text = mSelectedConstituency.name
                    tvConstituencyChange.visibility = View.VISIBLE
                    getCandidates(mSelectedConstituency.id)
                }

            }
            .addOnCompleteListener {
                loading = false
                dismissProgressDialogPopup()
            }

    }

    private fun getParty(stateID: String) {


        loading = true
        mPartyDataList.clear()
        showProgressDialog(getString(R.string.processing))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_PARTY)
        val query = ref
            .whereEqualTo(Constants.STATE_ID, stateID)
            .orderBy(Constants.DATE, Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                val mList: ArrayList<PartyModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    Log.d(" data", document.id + " => " + document.data)
                    val mCategoryData =
                        PartyModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.PARTY_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.PARTY_NAME)).toString()
                    mCategoryData.stateid =
                        (document.data?.get(Constants.STATE_ID)).toString()
                    mCategoryData.state =
                        (document.data?.get(Constants.STATE_NAME)).toString()
                    mCategoryData.isRegional =
                        (document.data?.get(Constants.PARTY_IS_REGIONAL)).toString()
                    mList.add(mCategoryData)
                    if (mIsEdit && !mStateChanged) {
                        if (mModel.partyid == mCategoryData.id) {
                            mSelectedParty = mCategoryData
                        }
                    }

                }
                mPartyDataList.addAll(mList)

                if (mPartyDataList.isEmpty()) {
                    mSelectedParty = PartyModel()
                    tvPartyName.text = getString(R.string.no_party_available)
                    tvPartyChange.visibility = View.GONE
                } else {
                    if (mSelectedParty.id == "") {
                        mSelectedParty = mPartyDataList[0]

                    }
                    tvPartyName.text = mSelectedParty.name
                    tvPartyChange.visibility = View.VISIBLE
                    if (mSelectedParty.name == "other") {
                        llLogo.visibility = View.VISIBLE
                    } else {
                        llLogo.visibility = View.GONE
                    }
                }

            }
            .addOnCompleteListener {
                loading = false
                dismissProgressDialogPopup()
                getConstituency(mSelectedState.id)
            }

    }

    private fun getCandidates(constituencyID: String) {

        if (mPartyDataList.isNotEmpty()) {
            loading = true
            mCandidateList.clear()
            showProgressDialog(getString(R.string.processing))
            val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
            val ref = db!!.collection(Constants.COLLECTION_CANDIDATE)
            val query = ref
                .whereEqualTo(Constants.CONSTITUENCY_ID, constituencyID)
                .orderBy(Constants.DATE, Query.Direction.DESCENDING)

            query.get()
                .addOnSuccessListener { fetchall_task ->

                    val mList: ArrayList<CandidateModel> = arrayListOf()
                    for (document in fetchall_task.documents) {
                        Log.d(" data", document.id + " => " + document.data)
                        val mCategoryData =
                            CandidateModel()
                        mCategoryData.id = document.id
                        mCategoryData.image =
                            (document.data?.get(Constants.CANDIDATE_IMAGE)).toString()
                        mCategoryData.name =
                            (document.data?.get(Constants.CANDIDATE_NAME)).toString()
                        mCategoryData.logo =
                            (document.data?.get(Constants.LOGO)).toString()
                        mCategoryData.state = mSelectedState.name
                        mCategoryData.stateid =
                            (document.data?.get(Constants.STATE_ID)).toString()
                        mCategoryData.constituency = mSelectedConstituency.name
                        mCategoryData.constituencyid =
                            (document.data?.get(Constants.CONSTITUENCY_ID)).toString()
                        mCategoryData.partyid =
                            (document.data?.get(Constants.PARTY_ID)).toString()
                        val partyname = if (mPartyDataList.isNotEmpty()) {
                            mPartyDataList.filter { it.id == mCategoryData.partyid }.single().name
                        } else {
                            ""
                        }
                        mCategoryData.party = partyname
                        val partylogo = if (mPartyDataList.isNotEmpty()) {
                            mPartyDataList.filter { it.id == mCategoryData.partyid }.single().image
                        } else {
                            ""
                        }
                        mCategoryData.partyimage = partylogo
                        mList.add(mCategoryData)


                    }
                    mCandidateList.addAll(mList)


                }
                .addOnCompleteListener {
                    loading = false
                    dismissProgressDialogPopup()
                }
        } else {
            showMessage(this, getString(R.string.no_party))
        }


    }

    private fun checkAndRequestPermissions(): Boolean {
        val writepermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val readpermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        val camerpermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)


        val listPermissionsNeeded = ArrayList<String>()

        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (readpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        if (camerpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }

        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                listPermissionsNeeded.toTypedArray(),
                REQUEST_PERMISSION
            )
            return false
        }
        return true
    }

    private fun pickImage(isImage: Boolean) {


        val infoDialog: Dialog? = createCustomDialog(
            this@AddCandidateActivity, R.layout.layout_select_photo,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )
        infoDialog!!.findViewById<TextView>(R.id.tv_select_camera).setOnClickListener {
            infoDialog.dismiss()
            val writepermission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            val readpermission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            val camerpermission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)

            if (writepermission == PackageManager.PERMISSION_GRANTED && readpermission == PackageManager.PERMISSION_GRANTED && camerpermission == PackageManager.PERMISSION_GRANTED) {
                openCamera(isImage)
            } else {
                checkAndRequestPermissions()
            }


        }
        infoDialog.findViewById<TextView>(R.id.tv_select_gallery).setOnClickListener {
            infoDialog.dismiss()
            val writepermission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            val readpermission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            val camerpermission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)

            if (writepermission == PackageManager.PERMISSION_GRANTED && readpermission == PackageManager.PERMISSION_GRANTED && camerpermission == PackageManager.PERMISSION_GRANTED) {
                openGallery(isImage)
            } else {
                checkAndRequestPermissions()
            }


        }
        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()


        }
        infoDialog.show()
    }

    private fun openCamera(isImage: Boolean) {
        val pictureIntent = Intent(
            MediaStore.ACTION_IMAGE_CAPTURE
        )
        if (pictureIntent.resolveActivity(packageManager) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile()

                photoURI = FileProvider.getUriForFile(
                    this,
                    "com.fivs.electionadminapp.provider",
                    photoFile
                )
                pictureIntent.putExtra(
                    MediaStore.EXTRA_OUTPUT,
                    photoURI
                )
                if (isImage) {
                    startActivityForResult(pictureIntent, CAPTURE_IMAGE)
                } else {
                    startActivityForResult(pictureIntent, CAPTURE_LOGO)
                }

            } catch (ex: IOException) {
            }

        }
    }

    private fun createImageFile(): File {
        val timeStamp =
            SimpleDateFormat(
                "yyyyMMdd_HHmmss",
                Locale.getDefault()
            ).format(Date())
        val imageFileName = "IMG_" + timeStamp + "_"
        val storageDir: File? =
            getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image: File = File.createTempFile(
            imageFileName,
            ".jpg",
            storageDir
        )

        imagePath = image.absolutePath
        return image
    }

    private fun openGallery(isImage: Boolean) {
        val getIntent = Intent(Intent.ACTION_GET_CONTENT)
        getIntent.type = "image/*"

        val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        pickIntent.type = "image/*"

        val chooserIntent = Intent.createChooser(getIntent, "Select Image")
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))

        if (isImage) {
            startActivityForResult(pickIntent, PICK_IMAGE)
        } else {
            startActivityForResult(pickIntent, PICK_LOGO)
        }
    }

    private fun pickState() {


        val infoDialog: Dialog? = createCustomDialog(
            this@AddCandidateActivity, R.layout.layout_select_state,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )


        infoDialog!!.findViewById<RecyclerView>(R.id.rvStateList).layoutManager =
            LinearLayoutManager(this)
        val mAdapter = AdapterDialogStateList(this, object :
            StateClickCallback {
            override fun onItemClick(position: Int) {
                mSelectedState = mStateDataList[position]
                tvStateName.text = mSelectedState.name
                mStateChanged = true
                mSelectedConstituency = ConstituencyModel()
                mSelectedParty = PartyModel()
                getParty(mSelectedState.id)
                infoDialog.dismiss()

            }

            override fun onImageClick(position: Int) {
            }

            override fun onItemRemoveClick(position: Int) {
            }

        }, mStateDataList)
        infoDialog.findViewById<RecyclerView>(R.id.rvStateList).adapter = mAdapter

        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()


        }
        infoDialog.show()
    }

    private fun pickConstituency() {


        val infoDialog: Dialog? = createCustomDialog(
            this@AddCandidateActivity, R.layout.layout_select_state,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )

        infoDialog!!.findViewById<TextView>(R.id.ivHeader).text =
            getString(R.string.select_constituency)
        infoDialog!!.findViewById<RecyclerView>(R.id.rvStateList).layoutManager =
            LinearLayoutManager(this)
        val mAdapter = AdapterDialogConstituencyList(this, object :
            ConstituencyClickCallback {
            override fun onItemClick(position: Int) {
                mSelectedConstituency = mConstituencyDataList[position]
                tvConstituencyName.text = mSelectedConstituency.name
                getCandidates(mSelectedConstituency.id)
                infoDialog.dismiss()

            }

            override fun onImageClick(position: Int) {
            }

            override fun onItemRemoveClick(position: Int) {
            }

        }, mConstituencyDataList)
        infoDialog.findViewById<RecyclerView>(R.id.rvStateList).adapter = mAdapter

        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()


        }
        infoDialog.show()
    }

    private fun pickParty() {


        val infoDialog: Dialog? = createCustomDialog(
            this@AddCandidateActivity, R.layout.layout_select_state,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )

        infoDialog!!.findViewById<TextView>(R.id.ivHeader).text = getString(R.string.select_party)
        infoDialog!!.findViewById<RecyclerView>(R.id.rvStateList).layoutManager =
            LinearLayoutManager(this)
        val mAdapter = AdapterDialogPartyList(this, object :
            PartyClickCallback {
            override fun onItemClick(position: Int) {
                if (mCandidateList.isEmpty() || mCandidateList.filter { it.partyid == mPartyDataList[position].id }.isEmpty()) {
                    mSelectedParty = mPartyDataList[position]
                    tvPartyName.text = mSelectedParty.name
                    if (mSelectedParty.name == "other") {
                        llLogo.visibility = View.VISIBLE
                    } else {
                        llLogo.visibility = View.GONE
                    }
                    infoDialog.dismiss()
                }


            }

            override fun onImageClick(position: Int) {
            }

            override fun onItemRemoveClick(position: Int) {
            }

        }, mPartyDataList)
        infoDialog.findViewById<RecyclerView>(R.id.rvStateList).adapter = mAdapter

        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()


        }
        infoDialog.show()
    }

    private fun showDoneMessage() {

        fireBroadcaste()

        val infoDialog: Dialog? = createCustomDialog(
            this@AddCandidateActivity, R.layout.layout_message,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )
        infoDialog!!.findViewById<TextView>(R.id.tv_message).text =
            getString(R.string.action_excecuted_sucess)
        infoDialog.findViewById<TextView>(R.id.tv_cancel).text = getString(R.string.ok)
        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()
            finish()
        }
        infoDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return
            } else {
                val imageUri: Uri? = data.data
                mImageImage = imageUri
                mImageFile = File(getRealPathFromURIPath(mImageImage!!, this).toString())

                Glide.with(this)
                    .load(imageUri)
                    .into(ivImage)
            }

        } else if (requestCode == PICK_LOGO && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return
            } else {
                val imageUri: Uri? = data.data
                mImageLogo = imageUri
                mLogoFile = File(getRealPathFromURIPath(mImageImage!!, this).toString())

                Glide.with(this)
                    .load(imageUri)
                    .into(ivLogo)
            }

        } else if (requestCode == CAPTURE_IMAGE && resultCode == Activity.RESULT_OK) {

            val imageUri: Uri? = photoURI

            mImageImage = imageUri
            mImageFile = photoFile

            Glide.with(this)
                .load(imageUri)
                .into(ivImage)
        } else if (requestCode == CAPTURE_LOGO && resultCode == Activity.RESULT_OK) {

            val imageUri: Uri? = photoURI

            mImageLogo = imageUri
            mLogoFile = photoFile

            Glide.with(this)
                .load(imageUri)
                .into(ivLogo)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_PERMISSION) {
            when {
                grantResults.isEmpty() -> // If user interaction was interrupted, the permission request is cancelled and you
                    // receive empty arrays.
                    Log.i("permission", "User interaction was cancelled.")
                grantResults[0] == PackageManager.PERMISSION_GRANTED -> // Permission was granted.
                {

                }
                else -> // Permission denied.
                {
                    Snackbar
                        .make(
                            btnAdd,
                            getString(R.string.please_allow_permission),
                            Snackbar.LENGTH_LONG
                        )
                        .setAction(R.string.ok) {

                            startActivity(
                                Intent(
                                    android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.parse("package:com.fivs.electionadminapp")
                                )
                            )

                        }
                        .show()
                }

            }
        }


    }


    private fun getRealPathFromURIPath(contentURI: Uri, activity: Activity): String? {
        val cursor = activity.contentResolver.query(contentURI, null, null, null, null)
        return if (cursor == null) {
            contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            if (idx == -1) {
                cursor.close()
                contentURI.path
            } else {
                val path = cursor.getString(idx)
                cursor.close()
                path
            }

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    private fun fireBroadcaste() {
        val localBroadcastManager: LocalBroadcastManager =
            LocalBroadcastManager.getInstance(this)
        val localIntent = Intent(Constants.ACTION_BROADCAST_FCM_RECIEVED)
        localBroadcastManager.sendBroadcast(localIntent)
    }
}
