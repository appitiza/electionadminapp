package com.fivs.electionadminapp.ui

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fivs.electionadminapp.util.Constants
import com.fivs.electionadminapp.R
import com.fivs.electionadminapp.adapter.AdapterDialogConstituencyList
import com.fivs.electionadminapp.adapter.AdapterDialogStateList
import com.fivs.electionadminapp.callback.ConstituencyClickCallback
import com.fivs.electionadminapp.callback.StateClickCallback
import com.fivs.electionadminapp.data.model.ConstituencyModel
import com.fivs.electionadminapp.data.model.FingerTransferData
import com.fivs.electionadminapp.data.model.StaffModel
import com.fivs.electionadminapp.data.model.StateModel
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.Blob
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_add_staff.*
import java.io.ByteArrayOutputStream
import java.util.*

class AddStaffActivity : BaseActivity() {


    private var SCAN_FINGER: Int = 4

    private val REQUEST_PERMISSION = 3

    private lateinit var mImageBitmap: Bitmap
    private lateinit var mEnrollTemplate: ByteArray
    private var mIsEdit = false
    private lateinit var mModel: StaffModel
    private lateinit var mStorageRef: StorageReference
    private var loading = false
    private var mStateDataList: ArrayList<StateModel> = arrayListOf()
    private var mSelectedState: StateModel = StateModel()
    private var mConstituencyDataList: ArrayList<ConstituencyModel> = arrayListOf()
    private var mSelectedConstituency: ConstituencyModel = ConstituencyModel()
    private lateinit var mAuth: FirebaseAuth

    private var mStateChanged = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_staff)
        setScreenTitle(R.string.add_staff)
        initializeFirebase()
        getIntentData()
        enableBack()
        checkAndRequestPermissions()
        setClick()
        if (isNetworkAvailable()) {
            getStates()
        } else {
            showMessage(this, getString(R.string.no_network))
        }
    }


    private fun initializeFirebase() {
        mAuth = FirebaseAuth.getInstance()
        mStorageRef = FirebaseStorage.getInstance().reference
    }

    private fun getIntentData() {
        if (intent != null) {
            if (intent.extras != null && intent.extras!!.getSerializable("data") != null) {
                mModel = intent.extras!!.getSerializable("data") as StaffModel
                mIsEdit = true
                var requestOptions = RequestOptions()
                requestOptions =
                    requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .error(R.drawable.image_placeholder)
                        .placeholder(R.drawable.image_placeholder)

                Glide.with(this)
                    .load(mModel.fingerimage)
                    .apply(requestOptions)
                    .into(ivFinger)



                if (mIsEdit) {
                    etEmail.setText(mModel.id)
                    etName.setText(mModel.name)
                    etPhone.setText(mModel.mobile)
                    btnAdd.text = getString(R.string.update)
                    etEmail.setFocusable(false)
                    etEmail.setFocusableInTouchMode(false)
                    etEmail.setEnabled(false)
                    etEmail.setCursorVisible(false)
                    etEmail.setKeyListener(null)
                    etPassword.visibility = View.GONE
                    btnResetPassword.visibility = View.VISIBLE
                    setScreenTitle(R.string.edit_voter)
                }
            }
        }
    }

    private fun setClick() {
        ivFinger.setOnClickListener {
            startActivityForResult(Intent(this, FingerScannerActivity::class.java), SCAN_FINGER)

        }

        btnAdd.setOnClickListener {

            if (isNetworkAvailable()) {
                if (mIsEdit) {
                    if (::mImageBitmap.isInitialized && ::mEnrollTemplate.isInitialized) {
                        if (etEmail.text.toString().trim().isNotEmpty()
                            && etName.text.toString().trim().isNotEmpty()
                            && etPhone.text.toString().trim().isNotEmpty()
                            && (etPhone.text.toString().length > 11)
                            && mSelectedState.id.isNotEmpty()
                            && mSelectedConstituency.id.isNotEmpty()
                        ) {
                            uploadImage()
                        } else {
                            showMessage(this, getString(R.string.fill_all))
                        }
                    } else {
                        if (etEmail.text.toString().trim().isNotEmpty()
                            && etName.text.toString().trim().isNotEmpty()
                            && etPhone.text.toString().trim().isNotEmpty()
                            && (etPhone.text.toString().length > 11)
                            && mSelectedState.id.isNotEmpty()
                            && mSelectedConstituency.id.isNotEmpty()
                        ) {
                            updateData(mModel.fingerimage, mModel.enrolledTemplete)
                        } else {
                            showMessage(this, getString(R.string.fill_all))
                        }
                    }
                } else {
                    if (::mImageBitmap.isInitialized && ::mEnrollTemplate.isInitialized) {
                        if (etEmail.text.toString().isNotEmpty()
                            && etName.text.toString().isNotEmpty()
                            && etPhone.text.toString().trim().isNotEmpty()
                            && (etPhone.text.toString().length > 11)
                            && etPassword.text.toString().isNotEmpty()
                            && mSelectedState.id.isNotEmpty()
                            && mSelectedConstituency.id.isNotEmpty()
                        ) {
                            performRegister()

                        } else {
                            showMessage(this, getString(R.string.fill_all))
                        }
                    } else {
                        showMessage(this, getString(R.string.image_missing))
                    }
                }
            } else {
                showMessage(this, getString(R.string.no_network))
            }


        }
        llState.setOnClickListener {
            if (mStateDataList.isNotEmpty())
                pickState()
        }
        llConstituency.setOnClickListener {
            if (mConstituencyDataList.isNotEmpty()) {
                pickConstituency()
            }
        }
        btnResetPassword.setOnClickListener {
            resetPassword()
        }
    }

    private fun resetPassword() {
        showProgressDialog(getString(R.string.processing))
        mAuth.sendPasswordResetEmail(mModel.id.trim())
            .addOnCompleteListener(this) { task ->

                dismissProgressDialogPopup()
                if (task.isSuccessful) {
                    //mLogin.mUser = mAuth!!.currentUser
                    Toast.makeText(this, "Success", Toast.LENGTH_LONG).show()

                } else {
                    Toast.makeText(this, task.exception?.message.toString(), Toast.LENGTH_LONG)
                        .show()
                }

            }
    }

    fun performRegister() {
        showProgressDialog(getString(R.string.processing))
        mAuth.createUserWithEmailAndPassword(
            etEmail.text.toString().trim(),
            etPassword.text.toString().trim()
        )
            .addOnFailureListener(this) { task ->
                dismissProgressDialogPopup()
                Toast.makeText(this, task.message.toString(), Toast.LENGTH_LONG)
                    .show()
            }
            .addOnCompleteListener {
                dismissProgressDialogPopup()
                if (it.isSuccessful) {
                    uploadImage()
                }


            }
    }

    private fun uploadImage() {



        showProgressDialog(getString(R.string.processing))
        loading = true
        val remoteImagepath = StringBuilder()
        remoteImagepath.append(etEmail.text.toString())
        remoteImagepath.append(".jpg")
        val currentRef =
            mStorageRef.child(Constants.COLLECTION_STAFF).child(remoteImagepath.toString())

        val baos = ByteArrayOutputStream()
        mImageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        val uploadTask = currentRef.putBytes(data)

        uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    Toast.makeText(this, task.exception.toString(), Toast.LENGTH_LONG).show()
                }
            }
            currentRef.downloadUrl
        }.addOnCompleteListener { task ->
            dismissProgressDialogPopup()
            loading = false
            if (task.isSuccessful) {
                val downloadUri = task.result

                if (mIsEdit) {
                    if (::mImageBitmap.isInitialized && ::mEnrollTemplate.isInitialized) {
                        if (etEmail.text.toString().isNotEmpty()
                            && etName.text.toString().isNotEmpty()
                            && etPhone.text.toString().isNotEmpty()
                            && (etPhone.text.toString().length > 11)
                            && mSelectedState.id.isNotEmpty()
                            && mSelectedConstituency.id.isNotEmpty()
                        ) {


                            updateData(downloadUri.toString(), mEnrollTemplate)
                        } else {
                            showMessage(this, getString(R.string.fill_all))
                        }
                    } else {
                        updateData(mModel.fingerimage, mModel.enrolledTemplete)
                    }
                } else {

                    uploadData(downloadUri.toString())


                }

            } else {
                showMessage(this, getString(R.string.failed))
            }
        }

    }

    private fun updateData(image: String, templete: ByteArray) {
        showProgressDialog(getString(R.string.processing))
        loading = true
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val map = HashMap<String, Any>()
        map[Constants.STAFF_EMAIL] = etEmail.text.toString().trim()
        map[Constants.STAFF_NAME] = etName.text.toString().trim()
        map[Constants.STAFF_MOBILE] = etPhone.text.toString().trim()
        map[Constants.STAFF_FINGER_IMAGE] = image
        map[Constants.STAFF_FINGER_TEMPLETE] = Blob.fromBytes(templete)
        map[Constants.STATE_ID] = mSelectedState.id
        map[Constants.CONSTITUENCY_ID] = mSelectedConstituency.id
        map[Constants.DATE] = FieldValue.serverTimestamp()

        db!!.collection(Constants.COLLECTION_STAFF)
            .document(mModel.id)
            .set(map)
            .addOnCompleteListener { reg_task ->
                dismissProgressDialogPopup()
                loading = false
                if (reg_task.isSuccessful) {
                    showDoneMessage()
                } else {
                    showMessage(this, getString(R.string.failed))
                }
            }
    }

    private fun uploadData(image: String) {

        showProgressDialog(getString(R.string.processing))
        loading = true
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val map = HashMap<String, Any>()
        map[Constants.STAFF_EMAIL] = etEmail.text.toString().trim()
        map[Constants.STAFF_NAME] = etName.text.toString().trim()
        map[Constants.STAFF_MOBILE] = etPhone.text.toString().trim()
        map[Constants.STAFF_FINGER_IMAGE] = image
        map[Constants.STAFF_FINGER_TEMPLETE] = Blob.fromBytes(mEnrollTemplate)
        map[Constants.STATE_ID] = mSelectedState.id
        map[Constants.CONSTITUENCY_ID] = mSelectedConstituency.id
        map[Constants.DATE] = FieldValue.serverTimestamp()

        db!!.collection(Constants.COLLECTION_STAFF)
            .document(etEmail.text.toString())
            .set(map)
            .addOnCompleteListener { reg_task ->
                dismissProgressDialogPopup()
                loading = false
                if (reg_task.isSuccessful) {
                    showDoneMessage()
                } else {
                    showMessage(this, getString(R.string.failed))
                }
            }
    }

    private fun getStates() {


        loading = true
        showProgressDialog(getString(R.string.processing))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_STATE)
        val query = ref
            .orderBy(Constants.DATE, Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                val mList: ArrayList<StateModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    Log.d(" data", document.id + " => " + document.data)
                    val mCategoryData =
                        StateModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.STATE_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.STATE_NAME)).toString()
                    mList.add(mCategoryData)
                    if (mIsEdit) {
                        if (mModel.stateid == mCategoryData.id) {
                            mSelectedState = mCategoryData
                        }
                    }

                }
                mStateDataList.addAll(mList)
                if (mStateDataList.isEmpty()) {
                    tvStateName.text = getString(R.string.no_state_available)
                    tvStateChange.visibility = View.GONE
                } else {
                    if (mSelectedState.id == "") {
                        mSelectedState = mStateDataList[0]

                    }
                    tvStateName.text = mSelectedState.name
                    getConstituency(mSelectedState.id)
                    tvStateChange.visibility = View.VISIBLE
                }

            }
            .addOnCompleteListener {
                loading = false
                dismissProgressDialogPopup()
            }

    }

    private fun getConstituency(stateID: String) {


        loading = true
        mConstituencyDataList.clear()
        showProgressDialog(getString(R.string.processing))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_CONSTITUENCY)
        val query = ref
            .whereEqualTo(Constants.STATE_ID, stateID)
            .orderBy(Constants.DATE, Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                val mList: ArrayList<ConstituencyModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    Log.d(" data", document.id + " => " + document.data)
                    val mCategoryData =
                        ConstituencyModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.CONSTITUENCY_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.CONSTITUENCY_NAME)).toString()
                    mCategoryData.stateid =
                        (document.data?.get(Constants.STATE_ID)).toString()
                    mCategoryData.state =
                        (document.data?.get(Constants.STATE_NAME)).toString()
                    mList.add(mCategoryData)

                    if (mIsEdit && !mStateChanged) {
                        if (mModel.constituencyid == mCategoryData.id) {
                            mSelectedConstituency = mCategoryData
                        }
                    }
                }
                mConstituencyDataList.addAll(mList)
                if (mConstituencyDataList.isEmpty()) {
                    tvConstituencyName.text = getString(R.string.no_constituency_available)
                    tvConstituencyChange.visibility = View.GONE
                } else {

                    if (mSelectedConstituency.id == "") {
                        mSelectedConstituency = mConstituencyDataList[0]

                    }
                    tvConstituencyName.text = mSelectedConstituency.name
                    tvConstituencyChange.visibility = View.VISIBLE
                }

            }
            .addOnCompleteListener {
                loading = false
                dismissProgressDialogPopup()
            }

    }

    private fun checkAndRequestPermissions(): Boolean {
        val writepermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val readpermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        val camerpermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)


        val listPermissionsNeeded = ArrayList<String>()

        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (readpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        if (camerpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }

        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                listPermissionsNeeded.toTypedArray(),
                REQUEST_PERMISSION
            )
            return false
        }
        return true
    }


    private fun pickState() {


        val infoDialog: Dialog? = createCustomDialog(
            this@AddStaffActivity, R.layout.layout_select_state,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )


        infoDialog!!.findViewById<RecyclerView>(R.id.rvStateList).layoutManager =
            LinearLayoutManager(this)
        val mAdapter = AdapterDialogStateList(this, object :
            StateClickCallback {
            override fun onItemClick(position: Int) {
                mStateChanged = true
                mSelectedConstituency = ConstituencyModel()
                mSelectedState = mStateDataList[position]
                tvStateName.text = mSelectedState.name
                getConstituency(mSelectedState.id)
                infoDialog.dismiss()

            }

            override fun onImageClick(position: Int) {
            }

            override fun onItemRemoveClick(position: Int) {
            }

        }, mStateDataList)
        infoDialog.findViewById<RecyclerView>(R.id.rvStateList).adapter = mAdapter

        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()


        }
        infoDialog.show()
    }

    private fun pickConstituency() {


        val infoDialog: Dialog? = createCustomDialog(
            this@AddStaffActivity, R.layout.layout_select_state,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )

        infoDialog!!.findViewById<TextView>(R.id.ivHeader).text = getString(R.string.select_constituency)
        infoDialog!!.findViewById<RecyclerView>(R.id.rvStateList).layoutManager =
            LinearLayoutManager(this)
        val mAdapter = AdapterDialogConstituencyList(this, object :
            ConstituencyClickCallback {
            override fun onItemClick(position: Int) {
                mSelectedConstituency = mConstituencyDataList[position]
                tvConstituencyName.text = mSelectedConstituency.name
                infoDialog.dismiss()

            }

            override fun onImageClick(position: Int) {
            }

            override fun onItemRemoveClick(position: Int) {
            }

        }, mConstituencyDataList)
        infoDialog.findViewById<RecyclerView>(R.id.rvStateList).adapter = mAdapter

        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()


        }
        infoDialog.show()
    }

    private fun showDoneMessage() {
        fireBroadcaste()

        val infoDialog: Dialog? = createCustomDialog(
            this@AddStaffActivity, R.layout.layout_message,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )
        infoDialog!!.findViewById<TextView>(R.id.tv_message).text =
            getString(R.string.action_excecuted_sucess)
        infoDialog.findViewById<TextView>(R.id.tv_cancel).text = getString(R.string.ok)
        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()
            finish()
        }
        infoDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SCAN_FINGER && resultCode == Activity.RESULT_OK) {


            if (data != null) {
                val fingerData: FingerTransferData =
                    data!!.getSerializableExtra("data") as FingerTransferData
                if (fingerData != null) {

                    val log = ("\nQuality: " + fingerData._Quality
                            + "\nNFIQ: " + fingerData._Nfiq
                            + "\nWSQ Compress Ratio: "
                            + fingerData._WSQCompressRatio
                            + "\nImage Dimensions (inch): "
                            + fingerData._InWidth + "\" X "
                            + fingerData._InHeight + "\""
                            + "\nImage Area (inch): " + fingerData._InArea
                            + "\"" + "\nResolution (dpi/ppi): "
                            + fingerData._Resolution + "\nGray Scale: "
                            + fingerData._GrayScale + "\nBits Per Pixal: "
                            + fingerData._Bpp + "\nWSQ Info: "
                            + fingerData._WSQInfo)

                    // val imageUri: Uri? = photoURI
                    //Toast.makeText(this, log, Toast.LENGTH_SHORT).show()
                    ivLog.text = log

                    mImageBitmap = BitmapFactory.decodeByteArray(
                        fingerData._FingerImage, 0,
                        fingerData._FingerImage.size
                    )

                    mEnrollTemplate = ByteArray(fingerData._ISOTemplate.size)
                    System.arraycopy(
                        fingerData._ISOTemplate, 0, mEnrollTemplate, 0,
                        fingerData._ISOTemplate.size
                    )
                    //mImage = imageUri
                    //mFile = photoFile

                    ivFinger.setImageBitmap(mImageBitmap)
                }

            } else {
                Toast.makeText(this, "Data is null", Toast.LENGTH_LONG).show()
            }

        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_PERMISSION) {
            when {
                grantResults.isEmpty() -> // If user interaction was interrupted, the permission request is cancelled and you
                    // receive empty arrays.
                    Log.i("permission", "User interaction was cancelled.")
                grantResults[0] == PackageManager.PERMISSION_GRANTED -> // Permission was granted.
                {

                }
                else -> // Permission denied.
                {
                    Snackbar
                        .make(
                            btnAdd,
                            getString(R.string.please_allow_permission),
                            Snackbar.LENGTH_LONG
                        )
                        .setAction(R.string.ok) {

                            startActivity(
                                Intent(
                                    android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.parse("package:com.fivs.electionadminapp")
                                )
                            )

                        }
                        .show()
                }

            }
        }


    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    private fun fireBroadcaste() {
        val localBroadcastManager: LocalBroadcastManager =
            LocalBroadcastManager.getInstance(this)
        val localIntent = Intent(Constants.ACTION_BROADCAST_FCM_RECIEVED)
        localBroadcastManager.sendBroadcast(localIntent)
    }
}
