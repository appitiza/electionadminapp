package com.fivs.electionadminapp.ui

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fivs.electionadminapp.util.Constants
import com.fivs.electionadminapp.R
import com.fivs.electionadminapp.data.model.StateModel
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_add_state.*
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class AddStateActivity : BaseActivity() {
    private var PICK_IMAGE: Int = 0
    private var CAPTURE_IMAGE: Int = 1
    private lateinit var photoURI: Uri
    private lateinit var photoFile: File
    private var imagePath = ""
    private val REQUEST_PERMISSION = 3

    private var mImage: Uri? = Uri.EMPTY
    private lateinit var mFile: File
    private var mIsEdit = false
    private lateinit var mModel: StateModel
    private lateinit var mStorageRef: StorageReference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_state)
        setScreenTitle(R.string.add_state)
        initializeFirebase()
        getIntentData()
        enableBack()
        checkAndRequestPermissions()
        setClick()
    }


    private fun initializeFirebase() {
        mStorageRef = FirebaseStorage.getInstance().reference
    }

    private fun getIntentData() {
        if (intent != null) {
            if (intent.extras != null && intent.extras!!.getSerializable("data") != null) {
                mModel = intent.extras!!.getSerializable("data") as StateModel
                if (mModel != null) {
                    mIsEdit = true
                    var requestOptions = RequestOptions()
                    requestOptions =
                        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)
                            .error(R.drawable.image_placeholder)
                            .placeholder(R.drawable.image_placeholder)
                    Glide.with(this)
                        .load(mModel.image)
                        .apply(requestOptions)
                        .into(ivImage)

                    if (mIsEdit) {
                        etName.setText(mModel.name)
                        btnAdd.text = getString(R.string.update)
                        setScreenTitle(R.string.edit_state)
                    }
                }
            }
        }
    }

    private fun setClick() {
        ivImage.setOnClickListener {
            pickImage()
        }
        btnAdd.setOnClickListener {
            if (isNetworkAvailable()) {
                if (mIsEdit) {
                    if (::mFile.isInitialized) {
                        if (etName.text.toString().trim().isNotEmpty()) {
                            uploadImage()
                        } else {
                            showMessage(this, getString(R.string.fill_all))
                        }
                    } else {
                        if (etName.text.toString().trim().isNotEmpty()) {
                            updateData(mModel.image)
                        } else {
                            showMessage(this, getString(R.string.fill_all))
                        }
                    }
                } else {
                    if (::mFile.isInitialized) {
                        if (etName.text.toString().isNotEmpty()) {
                            uploadImage()
                        } else {
                            showMessage(this, getString(R.string.fill_all))
                        }
                    } else {
                        showMessage(this, getString(R.string.image_missing))
                    }
                }
            } else {
                showMessage(this, getString(R.string.no_network))
            }


        }
    }


    private fun uploadImage() {

        showProgressDialog(getString(R.string.processing))
        val remoteImagepath = StringBuilder()
        remoteImagepath.append(FirebaseFirestore.getInstance().collection(Constants.COLLECTION_PARTY).document().id+"_"+etName.text.toString().trim())
        remoteImagepath.append(".jpg")
        val currentRef =
            mStorageRef.child(Constants.COLLECTION_STATE).child(remoteImagepath.toString())

        if (mFile.exists()) {
            val stream = FileInputStream(mFile)

            val uploadTask = currentRef.putStream(stream)




            uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        Toast.makeText(this, task.exception.toString(), Toast.LENGTH_LONG).show()
                    }
                }
                currentRef.downloadUrl
            }.addOnCompleteListener { task ->
                dismissProgressDialogPopup()
                if (task.isSuccessful) {
                    val downloadUri = task.result

                    if (mIsEdit) {
                        updateData(downloadUri.toString())
                    } else {
                        uploadData(downloadUri.toString())
                    }

                } else {
                    showMessage(this, getString(R.string.failed))
                }
            }
        }
        else
        {
            showMessage(this, getString(R.string.issue_inaccessing_file))
        }


    }

    private fun updateData(image: String) {
        showProgressDialog(getString(R.string.processing))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val map = HashMap<String, Any>()
        map[Constants.STATE_NAME] = etName.text.toString().trim()
        map[Constants.STATE_IMAGE] = image
        map[Constants.STATE_CAST_VOTE] = 0
        map[Constants.STATE_VOTER_COUNT] = 0
        map[Constants.STATE_WON_PARTY] = ""
        map[Constants.DATE] = FieldValue.serverTimestamp()

        db!!.collection(Constants.COLLECTION_STATE)
            .document(mModel.id)
            .set(map)
            .addOnCompleteListener { reg_task ->
                dismissProgressDialogPopup()
                if (reg_task.isSuccessful) {
                    showDoneMessage()
                } else {
                    showMessage(this, getString(R.string.failed))
                }
            }
    }

    private fun uploadData(image: String) {

        showProgressDialog(getString(R.string.processing))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val map = HashMap<String, Any>()
        map[Constants.STATE_NAME] = etName.text.toString().trim()
        map[Constants.STATE_IMAGE] = image
        map[Constants.STATE_CAST_VOTE] = 0
        map[Constants.STATE_VOTER_COUNT] = 0
        map[Constants.STATE_WON_PARTY] = ""
        map[Constants.DATE] = FieldValue.serverTimestamp()

        db!!.collection(Constants.COLLECTION_STATE)
            .add(map)
            .addOnCompleteListener { reg_task ->
                dismissProgressDialogPopup()
                if (reg_task.isSuccessful) {
                    showDoneMessage()
                } else {
                    showMessage(this, getString(R.string.failed))
                }
            }
    }

    private fun checkAndRequestPermissions(): Boolean {
        val writepermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val readpermission =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
        val camerpermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)


        val listPermissionsNeeded = ArrayList<String>()

        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (readpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        if (camerpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }

        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                listPermissionsNeeded.toTypedArray(),
                REQUEST_PERMISSION
            )
            return false
        }
        return true
    }

    private fun pickImage() {


        val infoDialog: Dialog? = createCustomDialog(
            this@AddStateActivity, R.layout.layout_select_photo,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )
        infoDialog!!.findViewById<TextView>(R.id.tv_select_camera).setOnClickListener {
            infoDialog.dismiss()
            val writepermission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            val readpermission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            val camerpermission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)

            if (writepermission == PackageManager.PERMISSION_GRANTED && readpermission == PackageManager.PERMISSION_GRANTED && camerpermission == PackageManager.PERMISSION_GRANTED) {
                openCamera()
            } else {
                checkAndRequestPermissions()
            }


        }
        infoDialog.findViewById<TextView>(R.id.tv_select_gallery).setOnClickListener {
            infoDialog.dismiss()
            val writepermission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            val readpermission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            val camerpermission =
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)

            if (writepermission == PackageManager.PERMISSION_GRANTED && readpermission == PackageManager.PERMISSION_GRANTED && camerpermission == PackageManager.PERMISSION_GRANTED) {
                openGallery()
            } else {
                checkAndRequestPermissions()
            }


        }
        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()


        }
        infoDialog.show()
    }

    fun openCamera() {
        val pictureIntent = Intent(
            MediaStore.ACTION_IMAGE_CAPTURE
        )
        if (pictureIntent.resolveActivity(packageManager) != null) {
            //Create a file to store the image

            try {
                photoFile = createImageFile()
                if (photoFile != null) {

                    photoURI = FileProvider.getUriForFile(
                        this,
                        "com.fivs.electionadminapp.provider",
                        photoFile
                    )
                    pictureIntent.putExtra(
                        MediaStore.EXTRA_OUTPUT,
                        photoURI
                    )

                    startActivityForResult(
                        pictureIntent,
                        CAPTURE_IMAGE
                    )
                }
            } catch (ex: IOException) {
            }

        }
    }

    fun createImageFile(): File {
        val timeStamp =
            SimpleDateFormat(
                "yyyyMMdd_HHmmss",
                Locale.getDefault()
            ).format(Date())
        val imageFileName = "IMG_" + timeStamp + "_"
        val storageDir: File? =
            getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image: File = File.createTempFile(
            imageFileName,
            ".jpg",
            storageDir
        )

        imagePath = image.absolutePath
        return image
    }

    fun openGallery() {
        val getIntent = Intent(Intent.ACTION_GET_CONTENT)
        getIntent.type = "image/*"

        val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        pickIntent.type = "image/*"

        val chooserIntent = Intent.createChooser(getIntent, "Select Image")
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))

        startActivityForResult(pickIntent, PICK_IMAGE)
    }

    private fun showDoneMessage() {
        fireBroadcaste()

        val infoDialog: Dialog? = createCustomDialog(
            this@AddStateActivity, R.layout.layout_message,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )
        infoDialog!!.findViewById<TextView>(R.id.tv_message).text =
            getString(R.string.action_excecuted_sucess)
        infoDialog.findViewById<TextView>(R.id.tv_cancel).text = getString(R.string.ok)
        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()
            finish()
        }
        infoDialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return
            } else {
                val imageUri: Uri? = data.data
                mImage = imageUri
                mFile = File(getRealPathFromURIPath(mImage!!, this).toString())

                Glide.with(this)
                    .load(imageUri)
                    .into(ivImage)
            }

        } else if (requestCode == CAPTURE_IMAGE && resultCode == Activity.RESULT_OK) {

            val imageUri: Uri? = photoURI

            mImage = imageUri
            mFile = photoFile

            Glide.with(this)
                .load(imageUri)
                .into(ivImage)
        }


        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_PERMISSION) {
            when {
                grantResults.isEmpty() -> // If user interaction was interrupted, the permission request is cancelled and you
                    // receive empty arrays.
                    Log.i("permission", "User interaction was cancelled.")
                grantResults[0] == PackageManager.PERMISSION_GRANTED -> // Permission was granted.
                {

                }
                else -> // Permission denied.
                {
                    Snackbar
                        .make(
                            btnAdd,
                            getString(R.string.please_allow_permission),
                            Snackbar.LENGTH_LONG
                        )
                        .setAction(R.string.ok) {

                            startActivity(
                                Intent(
                                    android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.parse("package:com.fivs.electionadminapp")
                                )
                            )

                        }
                        .show()
                }

            }
        }


    }


    private fun getRealPathFromURIPath(contentURI: Uri, activity: Activity): String? {
        val cursor = activity.contentResolver.query(contentURI, null, null, null, null)
        return if (cursor == null) {
            contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            if (idx == -1) {
                cursor.close()
                contentURI.path
            } else {
                val path = cursor.getString(idx)
                cursor.close()
                path
            }

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    private fun fireBroadcaste() {
        val localBroadcastManager: LocalBroadcastManager =
            LocalBroadcastManager.getInstance(this)
        val localIntent = Intent(Constants.ACTION_BROADCAST_FCM_RECIEVED)
        localBroadcastManager.sendBroadcast(localIntent)
    }
}
