package com.fivs.electionadminapp.ui

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.fivs.electionadminapp.util.Constants
import com.fivs.electionadminapp.R
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_status.*
import java.util.*

class ElectionStatusActivity : BaseActivity() {
    private var isElectionBlocked = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_status)
        enableBack()
        initializeComponent()
        setClick()
        getElectionStatua()

    }

    private fun initializeComponent() {

    }

    fun setClick() {

        btnAdd.setOnClickListener {
            uploadData()
        }
    }

    fun getElectionStatua() {

        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_ELECTION_STATUS)
        val query = ref
            .document(Constants.ELECTION_STATUS_KEY)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                if ((fetchall_task.data?.get(Constants.ELECTION_BLOCKED)).toString() != "null") {
                    isElectionBlocked =
                        (fetchall_task.data?.get(Constants.ELECTION_BLOCKED)).toString()
                }

                setSwitch()
            }
            .addOnCompleteListener {
                dismissProgressDialogPopup()
            }
    }

    private fun setSwitch() {
        if (isElectionBlocked.isNotEmpty()) {
            tvStatus.visibility = View.GONE
            btnAdd.text = getString(R.string.update_election_status)
            swBlocked.isChecked = isElectionBlocked == "1"
        } else {
            tvStatus.visibility = View.VISIBLE
            btnAdd.text = getString(R.string.add_election_status)
            tvStatus.text = getString(R.string.not_yet_set)
        }
    }

    private fun uploadData() {

        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val map = HashMap<String, Any>()
        map[Constants.ELECTION_BLOCKED] = if (swBlocked.isChecked) {
            "1"
        } else {
            "0"
        }

        map[Constants.DATE] = FieldValue.serverTimestamp()

        db!!.collection(Constants.COLLECTION_ELECTION_STATUS)
            .document(Constants.ELECTION_STATUS_KEY)
            .set(map)
            .addOnCompleteListener { reg_task ->
                dismissProgressDialogPopup()
                if (reg_task.isSuccessful) {
                    showDoneMessage()

                } else {
                    showMessage(this, getString(R.string.failed))
                }
            }
    }
    private fun showDoneMessage() {


        val infoDialog: Dialog? = createCustomDialog(
            this@ElectionStatusActivity, R.layout.layout_message,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )
        infoDialog!!.findViewById<TextView>(R.id.tv_message).text =
            getString(R.string.action_excecuted_sucess)
        infoDialog.findViewById<TextView>(R.id.tv_cancel).text = getString(R.string.ok)
        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()
            finish()
        }
        infoDialog.show()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(listenerDataChange, IntentFilter(Constants.ACTION_BROADCAST_FCM_RECIEVED))
    }
    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(listenerDataChange)
    }
    private val listenerDataChange = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Constants.ACTION_BROADCAST_FCM_RECIEVED -> {

                    getElectionStatua()

                }
            }
        }
    }
}