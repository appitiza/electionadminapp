package com.fivs.electionadminapp.ui

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.DatePicker
import android.widget.TextView
import android.widget.TimePicker
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.fivs.electionadminapp.util.Constants
import com.fivs.electionadminapp.R
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_election_time.*
import java.text.SimpleDateFormat
import java.util.*


class ElectionTimeActivity : BaseActivity() {
    private lateinit var startTime: Date
    private lateinit var endTime: Date
    private lateinit var resultTime: Date
    val format: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_election_time)
        enableBack()
        initializeComponent()
        setClick()
        getElectionTime()

    }

    private fun initializeComponent() {

    }

    fun setClick() {

        btnAdd.setOnClickListener {
            uploadData()
        }

        etStart.setOnClickListener {
            showDatePopUp(1)
        }
        etEnd.setOnClickListener {
            showDatePopUp(2)
        }
        etResult.setOnClickListener {
            showDatePopUp(3)
        }
    }

    fun getElectionTime() {

        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_ELECTION_TIME)
        val query = ref
            .document(Constants.ELECTION_TIME_KEY)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                if ((fetchall_task.data?.get(Constants.ELECTION_START_TIME)).toString() != "null") {

                    tvTimeStatus.visibility = View.GONE
                    btnAdd.text = getString(R.string.update_time)


                    val parser =
                        SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyy", Locale.getDefault())
                    val formatter = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())
                    val formattedStartDate =
                        formatter.format(parser.parse(fetchall_task.data?.get(Constants.ELECTION_START_TIME).toString()))
                    etStart.setText(formattedStartDate)


                    val formattedEndDate =
                        formatter.format(parser.parse(fetchall_task.data?.get(Constants.ELECTION_END_TIME).toString()))
                    etEnd.setText(formattedEndDate)

                    val formattedResultDate =
                        formatter.format(parser.parse(fetchall_task.data?.get(Constants.ELECTION_RESULT_TIME).toString()))
                    etResult.setText(formattedResultDate)

                } else {
                    tvTimeStatus.visibility = View.VISIBLE
                    btnAdd.text = getString(R.string.add_election_time)
                }
            }
            .addOnCompleteListener {
                dismissProgressDialogPopup()
            }
    }


    private fun uploadData() {

        if (::startTime.isInitialized && ::endTime.isInitialized && ::resultTime.isInitialized) {
            if (startTime.before(endTime)) {

                if (endTime.before(resultTime)) {
                    showProgressDialog(getString(R.string.loading))
                    val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
                    val map = HashMap<String, Any>()
                    map[Constants.ELECTION_START_TIME] = startTime.toString()
                    map[Constants.ELECTION_END_TIME] = endTime.toString()
                    map[Constants.ELECTION_RESULT_TIME] = resultTime.toString()

                    map[Constants.DATE] = FieldValue.serverTimestamp()

                    db!!.collection(Constants.COLLECTION_ELECTION_TIME)
                        .document(Constants.ELECTION_TIME_KEY)
                        .set(map)
                        .addOnCompleteListener { reg_task ->
                            dismissProgressDialogPopup()
                            if (reg_task.isSuccessful) {
                                showDoneMessage()

                            } else {
                                showMessage(this, getString(R.string.failed))
                            }
                        }
                }
                else {
                    showMessage(this, getString(R.string.result_should_be_before))
                }
            } else {
                showMessage(this, getString(R.string.start_should_be_before))
            }
        } else {
            showMessage(this, getString(R.string.initialize_both_date))
        }


    }

    fun showDatePopUp(type: Int) {
        val infoDialog: Dialog? = createCustomDialog(
            this, R.layout.datetime_picker_layout,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )



        infoDialog!!.findViewById<TextView>(R.id.date_time_set).setOnClickListener {

            val datePicker = infoDialog.findViewById(R.id.date_picker) as DatePicker
            val timePicker = infoDialog.findViewById(R.id.time_picker) as TimePicker


            var month = (datePicker.month + 1).toString()
            if (month.length == 1) {
                month = "0" + month
            }

            var day = datePicker.dayOfMonth.toString()
            if (day.length == 1) {
                day = "0" + day
            }

            var hour = timePicker.hour.toString()
            if (hour.length == 1) {
                hour = "0" + hour
            }

            var minute = timePicker.minute.toString()
            if (minute.length == 1) {
                minute = "0" + minute
            }

            val datetime = StringBuilder()
            datetime.append(datePicker.year)
            datetime.append("-")
            datetime.append(month)
            datetime.append("-")
            datetime.append(day)
            datetime.append("T")
            datetime.append(hour)
            datetime.append(":")
            datetime.append(minute)
            datetime.append(":01")


            val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
            val formatter = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())
            val formattedDate = formatter.format(parser.parse(datetime.toString()))
            if (type == 1) {

                startTime = parser.parse(datetime.toString())
                etStart.setText(formattedDate)
            } else if (type == 2) {
                endTime = parser.parse(datetime.toString())
                etEnd.setText(formattedDate)
            } else {
                resultTime = parser.parse(datetime.toString())
                etResult.setText(formattedDate)
            }




            infoDialog.dismiss()


        }
        infoDialog.show()
    }
    private fun showDoneMessage() {


        val infoDialog: Dialog? = createCustomDialog(
            this@ElectionTimeActivity, R.layout.layout_message,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )
        infoDialog!!.findViewById<TextView>(R.id.tv_message).text =
            getString(R.string.action_excecuted_sucess)
        infoDialog.findViewById<TextView>(R.id.tv_cancel).text = getString(R.string.ok)
        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()
            finish()
        }
        infoDialog.show()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                listenerDataChange,
                IntentFilter(Constants.ACTION_BROADCAST_FCM_RECIEVED)
            )
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(listenerDataChange)
    }

    private val listenerDataChange = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Constants.ACTION_BROADCAST_FCM_RECIEVED -> {

                    getElectionTime()

                }
            }
        }
    }

}