package com.fivs.electionadminapp.ui

import android.content.Intent
import android.os.Bundle
import com.fivs.electionadminapp.util.Constants
import com.fivs.electionadminapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {
    private var mAuth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initialize()
        setClick()
    }

    override fun onStart() {
        super.onStart()
    }
    fun initialize() {
        mAuth = FirebaseAuth.getInstance()
        hideToolbar()
    }

    fun setClick() {
        btnLogin.setOnClickListener {
            if(isNetworkAvailable()) {
                if (etEmail.text.toString().trim().isNotEmpty() && etPassword.text.toString().trim().isNotEmpty()) {

                    performLogin()
                } else {
                    showMessage(this,getString(R.string.fill_all))
                }
            }
            else{
                showMessage(this,getString(R.string.no_network))
            }


        }
        tvForgotPassword.setOnClickListener {

            if(isNetworkAvailable()) {
                if (etEmail.text.toString().trim().isNotEmpty()) {
                    performReset()
                } else {
                    showMessage(this,getString(R.string.fill_all))
                }
            }
            else{
                showMessage(this,getString(R.string.no_network))
            }

        }
    }

    fun performLogin() {
        showProgressDialog(getString(R.string.loading))
        mAuth?.signInWithEmailAndPassword(
            etEmail.text.toString().trim(),
            etPassword.text.toString().trim()
        )
            ?.addOnCompleteListener(this) { task ->

                dismissProgressDialogPopup()
                if (task.isSuccessful) {
                    getStaffInfo(mAuth!!.currentUser!!.email.toString())

                } else {
                    showMessage(this,task.exception?.message.toString())

                }
            }
    }

    fun performReset() {
        showProgressDialog(getString(R.string.loading))
        mAuth?.sendPasswordResetEmail(etEmail.text.toString().trim())
            ?.addOnCompleteListener(this) { task ->

                dismissProgressDialogPopup()
                if (task.isSuccessful) {
                    showMessage(this,getString(R.string.password_reset_sent))

                } else {
                    showMessage(this,task.exception?.message.toString())
                }

            }
    }

    private fun getStaffInfo(id: String) {

        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_ADMIN)
        val query = ref
            .document(id)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                if((fetchall_task.data?.get(Constants.ADMIN_EMAIL)).toString() == id)
                {
                    startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                    finish()
                }


            }
            .addOnFailureListener {
                startActivity(Intent(this@LoginActivity, LoginActivity::class.java))
                finish()
            }
            .addOnCompleteListener {
            }

    }
}
