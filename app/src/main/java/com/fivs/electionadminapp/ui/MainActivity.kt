package com.fivs.electionadminapp.ui

import android.content.Intent
import android.os.Bundle
import androidx.annotation.NonNull
import com.fivs.electionadminapp.R
import com.fivs.electionadminapp.util.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : BaseActivity() {

    private lateinit var mAuth: FirebaseAuth
    private lateinit var mAuthListener: AuthStateListener
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeFireBase()
        setScreenTitle(R.string.app_name)
        setClick()
    }

    override fun onStart() {
        super.onStart()

        mAuth.addAuthStateListener(mAuthListener)
    }

    override fun onResume() {
        super.onResume()
        mAuth.addAuthStateListener(mAuthListener)
    }

    override fun onStop() {
        super.onStop()
        mAuth.removeAuthStateListener(mAuthListener)
    }

    private fun initializeFireBase() {
        mAuth = FirebaseAuth.getInstance()

        mAuthListener = FirebaseAuth.AuthStateListener() {
            fun onAuthStateChanged(@NonNull firebaseAuth: FirebaseAuth) {
                val user = FirebaseAuth.getInstance().currentUser
                if (user == null) {
                    startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                    finish()
                }
            }
        }

    }

    fun setClick() {

        btnState.setOnClickListener {
            if (isNetworkAvailable()) {
                startActivity(Intent(this@MainActivity, StateListActivity::class.java))
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
        btnConstituency.setOnClickListener {
            if (isNetworkAvailable()) {
                startActivity(Intent(this@MainActivity, ConstituencyActivity::class.java))
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
        btnParty.setOnClickListener {
            if (isNetworkAvailable()) {
                startActivity(Intent(this@MainActivity, PartyListActivity::class.java))
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
        btnCandidates.setOnClickListener {
            if (isNetworkAvailable()) {
                startActivity(Intent(this@MainActivity, CandidatesListActivity::class.java))
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
        btnControllers.setOnClickListener {
            if (isNetworkAvailable()) {
                startActivity(Intent(this@MainActivity, StaffListActivity::class.java))
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
        btnVoter.setOnClickListener {
            if (isNetworkAvailable()) {
                startActivity(Intent(this@MainActivity, VoterListActivity::class.java))
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
        btnSetTime.setOnClickListener {
            if (isNetworkAvailable()) {
                startActivity(Intent(this@MainActivity, ElectionTimeActivity::class.java))
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
        btnAction.setOnClickListener {
            if (isNetworkAvailable()) {
                startActivity(Intent(this@MainActivity, ElectionStatusActivity::class.java))
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
        btnFingerTest.setOnClickListener {
            if (isNetworkAvailable()) {
                startActivity(Intent(this@MainActivity, MFS100Test::class.java))
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
        btnResult.setOnClickListener {
            if (isNetworkAvailable()) {
                startActivity(Intent(this@MainActivity, MFS100Test::class.java))
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
        btnResult.setOnClickListener {
            if (isNetworkAvailable()) {
                getElectionTime()
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
        btnLogout.setOnClickListener {
            if (isNetworkAvailable()) {
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                finish()
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
    }

    fun getElectionTime() {

        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val calendar = Calendar.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_ELECTION_TIME)
        val query = ref
            .document(Constants.ELECTION_TIME_KEY)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                if ((fetchall_task.data?.get(Constants.ELECTION_START_TIME)).toString() != "null") {
                    val parser =
                        SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyy", Locale.getDefault())


                    val result =
                        parser.parse(fetchall_task.data?.get(Constants.ELECTION_RESULT_TIME).toString())
                    if (result!!.before(calendar.time)) {
                        startActivity(Intent(this@MainActivity, ElectionResultActivity::class.java))
                    } else {
                        showMessage(this, getString(R.string.result_has_not_reached))
                    }

                } else {
                    showMessage(this, getString(R.string.result_has_not_reached))
                }

                dismissProgressDialogPopup()

            }
            .addOnFailureListener {
                dismissProgressDialogPopup()
                showMessage(this, getString(R.string.result_has_not_reached))
            }
    }
}
