package com.fivs.electionadminapp.ui

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fivs.electionadminapp.util.Constants
import com.fivs.electionadminapp.R
import com.fivs.electionadminapp.adapter.AdapterDialogStateList
import com.fivs.electionadminapp.adapter.AdapterPartyList
import com.fivs.electionadminapp.callback.PartyClickCallback
import com.fivs.electionadminapp.callback.StateClickCallback
import com.fivs.electionadminapp.data.model.PartyModel
import com.fivs.electionadminapp.data.model.StateModel
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_party_list.*

class PartyListActivity : BaseActivity(), PartyClickCallback {
    private var mDataList: ArrayList<PartyModel> = arrayListOf()
    private lateinit var mAdapter: AdapterPartyList
    private var loading = false
    private var mIsFirstFetch = true
    private var last = false
    private val LIMIT = 10L
    private lateinit var lastVisible: DocumentSnapshot

    private var mStateDataList: java.util.ArrayList<StateModel> = arrayListOf()
    private var mSelectedState : StateModel = StateModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_party_list)
        setScreenTitle(R.string.party_list)
        enableBack()
        initializeComponent()
        setClick()
        getStates()

    }

    private fun initializeComponent() {
        rvList.layoutManager = LinearLayoutManager(this)
        mAdapter = AdapterPartyList(this, this, mDataList)
        rvList.adapter = mAdapter

        rvList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView
                    .layoutManager as LinearLayoutManager?
                if (!loading && !last && linearLayoutManager!!.itemCount <= linearLayoutManager.findLastVisibleItemPosition() + 1) {
                    //getPartyList()
                }

            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
    }

    fun setClick() {
        itemsswipetorefresh.setOnRefreshListener {
            mDataList.clear()
            mAdapter.notifyDataSetChanged()
            mIsFirstFetch = true
            last = false
            //getPartyList()
            getCompletePartyList()
            itemsswipetorefresh.isRefreshing = false

        }
        fbAdd.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    AddpartyActivity::class.java
                )
            )
        }
        llState.setOnClickListener {
            if(mStateDataList.isNotEmpty())
                pickState() }
    }
    private fun getStates() {


        loading = true
        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_STATE)
        val query = ref
            .orderBy(Constants.DATE, Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                val mList: java.util.ArrayList<StateModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    Log.d(" data", document.id + " => " + document.data)
                    val mCategoryData =
                        StateModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.STATE_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.STATE_NAME)).toString()
                    mList.add(mCategoryData)



                }
                mStateDataList.addAll(mList)
                if(mStateDataList.isEmpty())
                {
                    dismissProgressDialogPopup()
                    tvStateName.text = getString(R.string.no_state_available)
                    tvChange.visibility = View.GONE
                }
                else{
                    if(mSelectedState.id == "")
                    {
                        mSelectedState = mStateDataList[0]
                        //getPartyList()
                        getCompletePartyList()

                    }
                    tvStateName.text = mSelectedState.name
                    tvChange.visibility = View.VISIBLE
                }

            }
            .addOnFailureListener {
                dismissProgressDialogPopup()
            }
            .addOnCompleteListener {
                loading = false
            }

    }
    fun getPartyList() {

        if (!mIsFirstFetch) {
            loading = true
            showProgressDialog(getString(R.string.loading))
            val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
            val ref = db!!.collection(Constants.COLLECTION_PARTY)
            val query = ref
                .whereEqualTo(Constants.STATE_ID, mSelectedState.id)
                .orderBy(Constants.DATE, Query.Direction.DESCENDING)
                .startAfter(lastVisible)
                .limit(LIMIT)

            query.get()
                .addOnSuccessListener { fetchall_task ->

                    val mList: ArrayList<PartyModel> = arrayListOf()
                    for (document in fetchall_task.documents) {
                        Log.d(" data", document.id + " => " + document.data)
                        val mCategoryData =
                            PartyModel()
                        mCategoryData.id = document.id
                        mCategoryData.image =
                            (document.data?.get(Constants.PARTY_IMAGE)).toString()
                        mCategoryData.name =
                            (document.data?.get(Constants.PARTY_NAME)).toString()
                        mCategoryData.stateid =
                            (document.data?.get(Constants.STATE_ID)).toString()
                        mCategoryData.state = mSelectedState.name
                        mCategoryData.isRegional =
                            (document.data?.get(Constants.PARTY_IS_REGIONAL)).toString()
                        mList.add(mCategoryData)


                    }
                    mDataList.addAll(mList)
                    if (fetchall_task.documents.isNotEmpty()) {
                        lastVisible = fetchall_task.documents[fetchall_task.size() - 1]
                    }
                    if (fetchall_task.documents.size < LIMIT) {
                        last = true
                    }
                    mAdapter.notifyDataSetChanged()
                    if(mDataList.isEmpty())
                    {
                        tvNodata.visibility = android.view.View.VISIBLE
                    }
                    else{
                        tvNodata.visibility = android.view.View.GONE
                    }
                }
                .addOnCompleteListener {
                    loading = false
                    dismissProgressDialogPopup()
                }
        }
        else {
            loading = true
            showProgressDialog(getString(R.string.loading))
            val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
            val ref = db!!.collection(Constants.COLLECTION_PARTY)
            val query = ref
                .whereEqualTo(Constants.STATE_ID, mSelectedState.id)
                .orderBy(Constants.DATE, Query.Direction.DESCENDING)
                .limit(LIMIT)

            query.get()
                .addOnSuccessListener { fetchall_task ->
                    val mList: ArrayList<PartyModel> = arrayListOf()
                    for (document in fetchall_task.documents) {
                        Log.d(" data", document.id + " => " + document.data)
                        val mCategoryData =
                            PartyModel()
                        mCategoryData.id = document.id
                        mCategoryData.image =
                            (document.data?.get(Constants.PARTY_IMAGE)).toString()
                        mCategoryData.name =
                            (document.data?.get(Constants.PARTY_NAME)).toString()
                        mCategoryData.stateid =
                            (document.data?.get(Constants.STATE_ID)).toString()
                        mCategoryData.state = mSelectedState.name
                        mCategoryData.isRegional =
                            (document.data?.get(Constants.PARTY_IS_REGIONAL)).toString()
                        mList.add(mCategoryData)


                    }
                    mDataList.addAll(mList)

                    if (fetchall_task.documents.isNotEmpty()) {
                        lastVisible = fetchall_task.documents[fetchall_task.size() - 1]
                    }
                    if (fetchall_task.documents.size < LIMIT) {
                        last = true
                    }
                    mIsFirstFetch = false
                    mAdapter.notifyDataSetChanged()

                    if(mDataList.isEmpty())
                    {
                        tvNodata.visibility = View.VISIBLE
                    }
                    else{
                        tvNodata.visibility = View.GONE
                    }

                }
                .addOnCompleteListener {
                    loading = false
                    dismissProgressDialogPopup()
                }
        }
    }
    fun getCompletePartyList() {

        loading = true
        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_PARTY)
        val query = ref
            .whereEqualTo(Constants.STATE_ID, mSelectedState.id)
            .orderBy(Constants.DATE, Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { fetchall_task ->
                val mList: ArrayList<PartyModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    Log.d(" data", document.id + " => " + document.data)
                    val mCategoryData =
                        PartyModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.PARTY_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.PARTY_NAME)).toString()
                    mCategoryData.stateid =
                        (document.data?.get(Constants.STATE_ID)).toString()
                    mCategoryData.state = mSelectedState.name
                    mCategoryData.isRegional =
                        (document.data?.get(Constants.PARTY_IS_REGIONAL)).toString()
                    mList.add(mCategoryData)


                }
                mDataList.clear()
                mDataList.addAll(mList)

                if (fetchall_task.documents.isNotEmpty()) {
                    lastVisible = fetchall_task.documents[fetchall_task.size() - 1]
                }
                if (fetchall_task.documents.size < LIMIT) {
                    last = true
                }
                mIsFirstFetch = false
                mAdapter.notifyDataSetChanged()

                if(mDataList.isEmpty())
                {
                    tvNodata.visibility = View.VISIBLE
                }
                else{
                    tvNodata.visibility = View.GONE
                }

            }
            .addOnCompleteListener {
                loading = false
                dismissProgressDialogPopup()
            }
    }
    private fun pickState() {


        val infoDialog: Dialog? = createCustomDialog(
            this, R.layout.layout_select_state,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )


        infoDialog!!.findViewById<RecyclerView>(R.id.rvStateList).layoutManager = LinearLayoutManager(this)
        var mAdapter: AdapterDialogStateList = AdapterDialogStateList(this,object:
            StateClickCallback {
            override fun onItemClick(position: Int) {
                mSelectedState = mStateDataList[position]
                tvStateName.text = mSelectedState.name
                mDataList.clear()
                mAdapter.notifyDataSetChanged()
                mIsFirstFetch = true
                last = false
                //getPartyList()
                getCompletePartyList()
                infoDialog.dismiss()

            }

            override fun onImageClick(position: Int) {
            }

            override fun onItemRemoveClick(position: Int) {
            }

        }, mStateDataList)
        infoDialog!!.findViewById<RecyclerView>(R.id.rvStateList).adapter = mAdapter

        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()


        }
        infoDialog.show()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    override fun onItemClick(position: Int) {
        val intent = Intent(this, AddpartyActivity::class.java)
        val mBundle = Bundle()
        mBundle.putSerializable("data", mDataList[position])
        intent.putExtras(mBundle)
        startActivity(intent)
    }

    override fun onImageClick(position: Int) {
        val intent = Intent(this, SingleImageActivity::class.java)
        val mBundle = Bundle()
        mBundle.putString("url", mDataList[position].image)
        intent.putExtras(mBundle)
        startActivity(intent)
    }

    override fun onItemRemoveClick(position: Int) {
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(listenerDataChange, IntentFilter(Constants.ACTION_BROADCAST_FCM_RECIEVED))
    }
    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(listenerDataChange)
    }
    private val listenerDataChange = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Constants.ACTION_BROADCAST_FCM_RECIEVED -> {

                    //getPartyList()
                    getCompletePartyList()

                }
            }
        }
    }
}
