package com.fivs.electionadminapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fivs.electionadminapp.R
import kotlinx.android.synthetic.main.activity_single_image.*

class SingleImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_image)
        getIntentData()
    }

    private fun getIntentData() {
        if (intent != null) {
          val  url: String = intent.extras!!.getString("url", "")
          if(url.isNotEmpty())
          {
              var requestOptions = RequestOptions()
              requestOptions =
                  requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.image_placeholder)
                      .placeholder(R.drawable.image_placeholder)
              Glide.with(this)
                  .load(url)
                  .apply(requestOptions)
                  .into(iv_zoom)
          }

        }
    }
}
