package com.fivs.electionadminapp.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fivs.electionadminapp.util.Constants
import com.fivs.electionadminapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.*

class SplashActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null
    private val delayTimeAfterApi: Long = 1500

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initializeFirebase()
        delaySplashScreen()
    }

    private fun initializeFirebase() {
        mAuth = FirebaseAuth.getInstance()
    }

    private fun delaySplashScreen() = GlobalScope.launch {

        if(mAuth!!.currentUser != null)
        {
            getStaffInfo(mAuth!!.currentUser!!.email.toString())

        }
        else{
            withContext(Dispatchers.Default) { delay(delayTimeAfterApi) }
            startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
            finish()
        }
    }

    private fun getStaffInfo(id: String) {

        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_ADMIN)
        val query = ref
            .document(id)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                if((fetchall_task.data?.get(Constants.ADMIN_EMAIL)).toString() == id)
                {
                    startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                    finish()
                }
                else{
                    startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                    finish()
                }

            }
            .addOnFailureListener {
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                finish()
            }
            .addOnCompleteListener {
            }

    }
}
