package com.fivs.electionadminapp.ui

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fivs.electionadminapp.util.Constants
import com.fivs.electionadminapp.R
import com.fivs.electionadminapp.adapter.AdapterStateList
import com.fivs.electionadminapp.callback.StateClickCallback
import com.fivs.electionadminapp.data.model.StateModel
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_state_list.*

class StateListActivity : BaseActivity(), StateClickCallback {
    private var mDataList: ArrayList<StateModel> = arrayListOf()
    private lateinit var mAdapter: AdapterStateList
    private var loading = false
    private var mIsFirstFetch = true
    private var last = false
    private val LIMIT = 10L
    private lateinit var lastVisible: DocumentSnapshot
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_state_list)
        enableBack()
        initializeComponent()
        setClick()
        //getStates()
        getCompleteStates()

    }

    private fun initializeComponent() {
        rvList.layoutManager = LinearLayoutManager(this)
        mAdapter = AdapterStateList(this, this@StateListActivity, mDataList)
        rvList.adapter = mAdapter

        rvList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView
                    .layoutManager as LinearLayoutManager?
                if (!loading && !last && linearLayoutManager!!.itemCount <= linearLayoutManager.findLastVisibleItemPosition() + 1) {
                    //getStates()
                    //getCompleteStates()
                }

            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
    }

    fun setClick() {
        itemsswipetorefresh.setOnRefreshListener {
            mDataList.clear()
            mAdapter.notifyDataSetChanged()
            mIsFirstFetch = true
            last = false
            //getStates()
            getCompleteStates()
            itemsswipetorefresh.isRefreshing = false

        }
        fbAdd.setOnClickListener {
            startActivity(
                Intent(
                    this@StateListActivity,
                    AddStateActivity::class.java
                )
            )
        }
    }

    fun getStates() {

        if (!mIsFirstFetch) {
            loading = true
            showProgressDialog(getString(R.string.loading))
            val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
            val ref = db!!.collection(Constants.COLLECTION_STATE)
            val query = ref
                .orderBy(Constants.DATE, Query.Direction.DESCENDING)
                .startAfter(lastVisible)
                .limit(LIMIT)

            query.get()
                .addOnSuccessListener { fetchall_task ->

                    val mList: ArrayList<StateModel> = arrayListOf()
                    for (document in fetchall_task.documents) {
                        Log.d(" data", document.id + " => " + document.data)
                        val mCategoryData =
                            StateModel()
                        mCategoryData.id = document.id
                        mCategoryData.image =
                            (document.data?.get(Constants.STATE_IMAGE)).toString()
                        mCategoryData.name =
                            (document.data?.get(Constants.STATE_NAME)).toString()

                        mCategoryData.cast_vote =
                            (document.data?.get(Constants.STATE_CAST_VOTE)).toString().toInt()
                        mCategoryData.voter_count =
                            (document.data?.get(Constants.STATE_VOTER_COUNT)).toString().toInt()
                        mCategoryData.won_party =
                            (document.data?.get(Constants.STATE_WON_PARTY)).toString()

                        mList.add(mCategoryData)


                    }
                    mDataList.addAll(mList)
                    if (fetchall_task.documents.isNotEmpty()) {
                        lastVisible = fetchall_task.documents[fetchall_task.size() - 1]
                    }
                    if (fetchall_task.documents.size < LIMIT) {
                        last = true
                    }
                    mAdapter.notifyDataSetChanged()
                    if(mDataList.isEmpty())
                    {
                        tvNodata.visibility = android.view.View.VISIBLE
                    }
                    else{
                        tvNodata.visibility = android.view.View.GONE
                    }
                }
                .addOnCompleteListener {
                    loading = false
                    dismissProgressDialogPopup()
                }
        }
        else {
            loading = true
            showProgressDialog(getString(R.string.loading))
            val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
            val ref = db!!.collection(Constants.COLLECTION_STATE)
            val query = ref
                .orderBy(Constants.DATE, Query.Direction.DESCENDING)
                .limit(LIMIT)

            query.get()
                .addOnSuccessListener { fetchall_task ->
                    val mList: ArrayList<StateModel> = arrayListOf()
                    for (document in fetchall_task.documents) {
                        Log.d(" data", document.id + " => " + document.data)
                        val mCategoryData =
                            StateModel()
                        mCategoryData.id = document.id
                        mCategoryData.image =
                            (document.data?.get(Constants.STATE_IMAGE)).toString()
                        mCategoryData.name =
                            (document.data?.get(Constants.STATE_NAME)).toString()

                        mCategoryData.cast_vote =
                            (document.data?.get(Constants.STATE_CAST_VOTE)).toString().toInt()
                        mCategoryData.voter_count =
                            (document.data?.get(Constants.STATE_VOTER_COUNT)).toString().toInt()
                        mCategoryData.won_party =
                            (document.data?.get(Constants.STATE_WON_PARTY)).toString()

                        mList.add(mCategoryData)


                    }
                    mDataList.addAll(mList)

                    if (fetchall_task.documents.isNotEmpty()) {
                        lastVisible = fetchall_task.documents[fetchall_task.size() - 1]
                    }
                    if (fetchall_task.documents.size < LIMIT) {
                        last = true
                    }
                    mIsFirstFetch = false
                    mAdapter.notifyDataSetChanged()

                    if(mDataList.isEmpty())
                    {
                        tvNodata.visibility = android.view.View.VISIBLE
                    }
                    else{
                        tvNodata.visibility = android.view.View.GONE
                    }

                }
                .addOnCompleteListener {
                    loading = false
                    dismissProgressDialogPopup()
                }
        }
    }
    fun getCompleteStates() {
        loading = true
        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_STATE)
        val query = ref
            .orderBy(Constants.DATE, Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { fetchall_task ->
                val mList: ArrayList<StateModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    Log.d(" data", document.id + " => " + document.data)
                    val mCategoryData =
                        StateModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.STATE_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.STATE_NAME)).toString()

                    mCategoryData.cast_vote =
                        (document.data?.get(Constants.STATE_CAST_VOTE)).toString().toInt()
                    mCategoryData.voter_count =
                        (document.data?.get(Constants.STATE_VOTER_COUNT)).toString().toInt()
                    mCategoryData.won_party =
                        (document.data?.get(Constants.STATE_WON_PARTY)).toString()

                    mList.add(mCategoryData)


                }
                mDataList.clear()
                mDataList.addAll(mList)

                if (fetchall_task.documents.isNotEmpty()) {
                    lastVisible = fetchall_task.documents[fetchall_task.size() - 1]
                }
                if (fetchall_task.documents.size < LIMIT) {
                    last = true
                }
                mIsFirstFetch = false
                mAdapter.notifyDataSetChanged()

                if(mDataList.isEmpty())
                {
                    tvNodata.visibility = android.view.View.VISIBLE
                }
                else{
                    tvNodata.visibility = android.view.View.GONE
                }

            }
            .addOnCompleteListener {
                loading = false
                dismissProgressDialogPopup()
            }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    override fun onItemClick(position: Int) {
        val intent = Intent(this, AddStateActivity::class.java)
        val mBundle = Bundle()
        mBundle.putSerializable("data", mDataList[position])
        intent.putExtras(mBundle)
        startActivity(intent)
    }

    override fun onImageClick(position: Int) {
        val intent = Intent(this, SingleImageActivity::class.java)
        val mBundle = Bundle()
        mBundle.putString("url", mDataList[position].image)
        intent.putExtras(mBundle)
        startActivity(intent)
    }

    override fun onItemRemoveClick(position: Int) {
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(listenerDataChange, IntentFilter(Constants.ACTION_BROADCAST_FCM_RECIEVED))
    }
    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(listenerDataChange)
    }
    private val listenerDataChange = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Constants.ACTION_BROADCAST_FCM_RECIEVED -> {

                    //getStates()
                    getCompleteStates()

                }
            }
        }
    }
}