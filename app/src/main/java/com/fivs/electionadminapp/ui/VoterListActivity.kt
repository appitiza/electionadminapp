package com.fivs.electionadminapp.ui

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fivs.electionadminapp.util.Constants
import com.fivs.electionadminapp.R
import com.fivs.electionadminapp.adapter.AdapterDialogConstituencyList
import com.fivs.electionadminapp.adapter.AdapterDialogStateList
import com.fivs.electionadminapp.adapter.AdapterVoterList
import com.fivs.electionadminapp.callback.ConstituencyClickCallback
import com.fivs.electionadminapp.callback.StateClickCallback
import com.fivs.electionadminapp.callback.VoterClickCallback
import com.fivs.electionadminapp.data.model.ConstituencyModel
import com.fivs.electionadminapp.data.model.StateModel
import com.fivs.electionadminapp.data.model.VoterModel
import com.google.firebase.firestore.Blob
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_candidate_list.*

class VoterListActivity : BaseActivity(), VoterClickCallback {
    private var mDataList: ArrayList<VoterModel> = arrayListOf()
    private lateinit var mAdapter: AdapterVoterList
    private var loading = false
    private var mStateDataList: java.util.ArrayList<StateModel> = arrayListOf()
    private var mSelectedState: StateModel = StateModel()
    private var mConstituencyDataList: java.util.ArrayList<ConstituencyModel> = arrayListOf()
    private var mSelectedConstituency: ConstituencyModel = ConstituencyModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_voter_list)
        setScreenTitle(R.string.voter_list)
        enableBack()
        initializeComponent()
        setClick()
        getStates()

    }

    private fun initializeComponent() {
        rvList.layoutManager = LinearLayoutManager(this)
        mAdapter = AdapterVoterList(this, this, mDataList)
        rvList.adapter = mAdapter

    }

    fun setClick() {
        itemsswipetorefresh.setOnRefreshListener {
            mDataList.clear()
            mAdapter.notifyDataSetChanged()
            if (mSelectedConstituency.id.isNotEmpty()) {
                getVoter(mSelectedConstituency.id)
            }
            itemsswipetorefresh.isRefreshing = false

        }

        llState.setOnClickListener {
            if (mStateDataList.isNotEmpty())
                pickState()
        }
        llConstituency.setOnClickListener {
            if (mConstituencyDataList.isNotEmpty()) {
                pickConstituency()
            }
        }
        fbAdd.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    AddVoterActivity::class.java
                )
            )
        }
    }

    private fun getStates() {


        loading = true
        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_STATE)
        val query = ref
            .orderBy(Constants.DATE, Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                dismissProgressDialogPopup()
                val mList: java.util.ArrayList<StateModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    Log.d(" data", document.id + " => " + document.data)
                    val mCategoryData =
                        StateModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.STATE_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.STATE_NAME)).toString()
                    mList.add(mCategoryData)


                }
                mStateDataList.addAll(mList)
                if (mStateDataList.isEmpty()) {
                    tvStateName.text = getString(R.string.no_state_available)
                    tvStateChange.visibility = View.GONE
                } else {
                    if (mSelectedState.id == "") {
                        mSelectedState = mStateDataList[0]
                        tvStateName.text = mSelectedState.name
                        getConstituency(mSelectedState.id)
                    }
                    tvStateChange.visibility = View.VISIBLE
                }

            }
            .addOnFailureListener { dismissProgressDialogPopup() }
            .addOnCompleteListener {
                loading = false
            }

    }

    private fun getConstituency(stateID: String) {


        loading = true
        mConstituencyDataList.clear()
        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_CONSTITUENCY)
        val query = ref
            .whereEqualTo(Constants.STATE_ID, stateID)
            .orderBy(Constants.DATE, Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                dismissProgressDialogPopup()
                val mList: ArrayList<ConstituencyModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    Log.d(" data", document.id + " => " + document.data)
                    val mCategoryData =
                        ConstituencyModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.CONSTITUENCY_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.CONSTITUENCY_NAME)).toString()
                    mCategoryData.stateid =
                        (document.data?.get(Constants.STATE_ID)).toString()
                    mCategoryData.state =
                        (document.data?.get(Constants.STATE_NAME)).toString()
                    mList.add(mCategoryData)


                }
                mConstituencyDataList.addAll(mList)
                if (mConstituencyDataList.isEmpty()) {
                    tvConstituencyName.text = getString(R.string.no_constituency_available)
                    tvConstituencyChange.visibility = View.GONE
                } else {
                    mSelectedConstituency = mConstituencyDataList[0]
                    tvConstituencyName.text = mSelectedConstituency.name
                    getVoter(mSelectedConstituency.id)

                    tvConstituencyChange.visibility = View.VISIBLE
                }

            }
            .addOnFailureListener { dismissProgressDialogPopup() }
            .addOnCompleteListener {
                loading = false
            }

    }


    private fun getVoter(constituencyID: String) {


        loading = true
        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_VOTER)
        val query = ref
            .whereEqualTo(Constants.CONSTITUENCY_ID, constituencyID)
            .orderBy(Constants.DATE, Query.Direction.DESCENDING)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                val mList: ArrayList<VoterModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    Log.d(" data", document.id + " => " + document.data)

                    val mCategoryData =
                        VoterModel()
                    mCategoryData.id = document.id
                    mCategoryData.name =
                        (document.data?.get(Constants.VOTER_NAME)).toString()
                    mCategoryData.age =
                        (document.data?.get(Constants.VOTER_AGE)).toString()
                    mCategoryData.mobile =
                        (document.data?.get(Constants.VOTER_MOBILE)).toString()
                    mCategoryData.sex =
                        (document.data?.get(Constants.VOTER_SEX)).toString()
                    mCategoryData.fingerimage =
                        (document.data?.get(Constants.VOTER_FINGER_IMAGE)).toString()
                    mCategoryData.stateid =
                        (document.data?.get(Constants.STATE_ID)).toString()
                    mCategoryData.state =
                        (document.data?.get(Constants.STATE_NAME)).toString()
                    mCategoryData.constituencyid =
                        (document.data?.get(Constants.CONSTITUENCY_ID)).toString()
                    mCategoryData.constituency =
                        (document.data?.get(Constants.CONSTITUENCY_NAME)).toString()
                    if(document.data?.get(Constants.VOTER_FINGER_TEMPLETE) != null) {
                        mCategoryData.enrolledTemplete =
                            ((document.data?.get(Constants.VOTER_FINGER_TEMPLETE)) as Blob).toBytes()
                    }
                    mList.add(mCategoryData)


                }
                mDataList.clear()
                mDataList.addAll(mList)
                mAdapter.notifyDataSetChanged()
                if (mDataList.isEmpty()) {
                    tvNodata.visibility = View.VISIBLE
                } else {
                    tvNodata.visibility = View.GONE
                }

            }
            .addOnCompleteListener {
                loading = false
                dismissProgressDialogPopup()
            }

    }

    private fun pickState() {


        val infoDialog: Dialog? = createCustomDialog(
            this@VoterListActivity, R.layout.layout_select_state,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )


        infoDialog!!.findViewById<RecyclerView>(R.id.rvStateList).layoutManager =
            LinearLayoutManager(this)
        val mAdapter: AdapterDialogStateList = AdapterDialogStateList(this, object :
            StateClickCallback {
            override fun onItemClick(position: Int) {
                mSelectedState = mStateDataList[position]
                tvStateName.text = mSelectedState.name
                mDataList.clear()
                mAdapter.notifyDataSetChanged()
                getConstituency(mSelectedState.id)
                infoDialog.dismiss()

            }

            override fun onImageClick(position: Int) {
            }

            override fun onItemRemoveClick(position: Int) {
            }

        }, mStateDataList)
        infoDialog.findViewById<RecyclerView>(R.id.rvStateList).adapter = mAdapter

        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()


        }
        infoDialog.show()
    }

    private fun pickConstituency() {


        val infoDialog: Dialog? = createCustomDialog(
            this@VoterListActivity, R.layout.layout_select_state,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )

        infoDialog!!.findViewById<TextView>(R.id.ivHeader).text = getString(R.string.select_constituency)
        infoDialog!!.findViewById<RecyclerView>(R.id.rvStateList).layoutManager =
            LinearLayoutManager(this)
        val mAdapter = AdapterDialogConstituencyList(this, object :
            ConstituencyClickCallback {
            override fun onItemClick(position: Int) {
                mSelectedConstituency = mConstituencyDataList[position]
                tvConstituencyName.text = mSelectedConstituency.name
                mDataList.clear()
                mAdapter.notifyDataSetChanged()
                getVoter(mSelectedConstituency.id)
                infoDialog.dismiss()

            }

            override fun onImageClick(position: Int) {
            }

            override fun onItemRemoveClick(position: Int) {
            }

        }, mConstituencyDataList)
        infoDialog!!.findViewById<RecyclerView>(R.id.rvStateList).adapter = mAdapter

        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()


        }
        infoDialog.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }

    override fun onItemClick(position: Int) {
        val intent = Intent(this, AddVoterActivity::class.java)
        val mBundle = Bundle()
        mBundle.putSerializable("data", mDataList[position])
        intent.putExtras(mBundle)
        startActivity(intent)
    }

    override fun onImageClick(position: Int) {
    }

    override fun onItemRemoveClick(position: Int) {
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                listenerDataChange,
                IntentFilter(Constants.ACTION_BROADCAST_FCM_RECIEVED)
            )
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(listenerDataChange)
    }

    private val listenerDataChange = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Constants.ACTION_BROADCAST_FCM_RECIEVED -> {

                    getVoter(mSelectedConstituency.id)

                }
            }
        }
    }
}
